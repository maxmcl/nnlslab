% Function to set-up options specific to
% the positive lasso modification of LARS.
%
% Inputs
%
% 'down'   --- downdating of the Cholesky factorization
%              is done using MATLAB code ('down' = 1, default),
%              C-code ('down' = 2).
%
% 'maxvar' --- Algorithm is stopped prematurely once 
%             'maxvar' variables are contained in the present
%              active set. Default: '0', i.e. no premature
%              stopping based on the size of the active set
%              is used. 
%
% 'itmax'  --- Algorithm is stopped prematurely after 'itmax'
%              iterations. Default: 'inf', i.e. there is no
%              upper bound on the number of iterations.    
% 


function options = opt_plars(varargin)

p = inputParser;

validdown = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('down',1,validdown);

validmaxvar = @(x)validateattributes(x,{'numeric'},{'scalar','integer','nonnegative'});
p.addParamValue('maxvar',0,validmaxvar);

validitmax = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('itmax', inf, validitmax);

p.parse(varargin{:});
options = p.Results;



