% Name of the NNLS solver:
% Positive lasso modification of LARS. 
% This code is an adaptation of MATLAB
% code for the LARS available from http://pmtksupport.googlecode.com/svn/trunk/lars/larsen.m
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_plars'.

function[out] = plars(A, b, options_general, options_specific)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if options_general.usegram
       
        switch options_specific.down
            case 1
                   [out] = plars_gram(A, b, options_general, options_specific);
            case 2
                   [out] = plars_gram_downc(A, b, options_general, options_specific);
        end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
else
    
    switch options_specific.down
            case 1
                   [out] = plars_no_gram(A, b, options_general, options_specific);
            case 2
                   [out] = plars_no_gram_downc(A, b, options_general, options_specific);
    end       
end    














