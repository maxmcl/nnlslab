function[out] = plars_no_gram_downc(A, b, options_general, options_specific)

% Positive lasso modification of the LARS (Efron et al., 2004, Section 3.4) 
% based on code of Karl Skoglund, IMM, DTU, kas@imm.dtu.dk
  
%%% Initialize objective -- global [possibly unused]
global f;
f = @(x) norm(A * x - b)^2;

%%% Initialize CPU time
t0 = tic;
%%%
[n p] = size(A);

%initialize variables
nvars = min(n, p);
maxk = options_specific.itmax; % Maximum number of iterations
maxvar = options_specific.maxvar; % early stopping
I = 1:p; % inactive set
P = []; % positive set
% we shall always use the Cholesky factorization
R = []; % Cholesky factorization R'R = X'X where R is upper triangular


lassocond = 0; % LASSO condition boolean
earlystopcond = 0; % Early stopping condition boolean
k = 0; % Iteration count
vars = 0; % Current number of variables
x = zeros(p, 1);

c = A' * b;


% grad = -w_
perf = getL(options_general.perfmeas, -c, x);
check = check_termination(t0, options_general, perf, -c, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

% check 
if check.stop
   out.xopt = x; 
   out.err = f(x);
   out.ConvSpeed = ConvSpeed; 
   out.check = check;
end


%%% LARS main loop
while vars < nvars && ~earlystopcond && k < maxk && (~check.stop)
  k = k + 1;
  [C j] = max(c(I));
  if C < eps
     out.xopt = x; 
     out.err = f(x);
     out.ConvSpeed = ConvSpeed; 
     out.check = check; 
     return; 
  end
  j = I(j); % add one variable at a time only.

  if ~lassocond % if a variable has been dropped, do one iteration with this configuration (don't add new one right away)
    R = cholinsert(R, A(:,j), A(:,P));
    P = [P j];
    I(I == j) = [];
    vars = vars + 1;
  end

    s = ones(vars, 1);
    
    
    GA1 = R\(R'\s);
    AA = 1/sqrt(sum(GA1));
    w = AA*GA1;
  
    u = A(:,P)*w; % equiangular direction (unit vector)
  if vars == nvars % if all variables active, go all the way to the lsq solution
    gamma = C/AA;
  else
    a = A'*u; % correlation between each variable and equiangular vector
    temp = (C - c(I))./(AA - a(I)); % note: only positive correlations matter.
    gamma = min([temp(temp > eps); C/AA]);
  end

  % LASSO modification
 
    lassocond = 0;
    temp = (-x(P)./w)';
    %findtemp = find(temp > 0);
    [gamma_tilde] = min([temp(temp > 0) gamma]);
    j = find(abs(temp - gamma_tilde) < eps);
    if gamma_tilde < gamma
      gamma = gamma_tilde;
      lassocond = 1;
    end
 
  x(P) = x(P) + gamma*w;
    
  % If LASSO condition satisfied, drop variable from active set
  if lassocond == 1
      lj = length(j);
      for jj=1:lj
        R = choldown(R,j(lj - jj + 1));
        I = [I P(j)];
      end
      P(j) = [];
      vars = vars - length(j); % note that in general one may have several drops at a time.
  end
 
  % Early stopping at specified number of variables
  if maxvar > 0
    earlystopcond = vars >= maxvar;
  end
  
  c = A'* (b - A(:,P) * x(P));
  if all(isfinite(x))
      perf = getL(options_general.perfmeas, -c, x);
      check = check_termination(t0, options_general, perf, -c, x, check.fprev, check.xprev);
      perf = perf(~isnan(perf));
      ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
      
  else
      break;
  end
end

out.xopt = x; 
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;


if k == maxk
  disp('LARS warning: Forced exit. Maximum number of iteration reached.');
end


%%% Fast Cholesky insert and remove functions
% Updates R in a Cholesky factorization R'R = X'X of a data matrix X. R is
% the current R matrix to be updated. x is a column vector representing the
% variable to be added and X is the data matrix containing the currently
% active variables (not including x).
function R = cholinsert(R, x, X)
diag_k = x'*x; % diagonal element k in X'X matrix
if isempty(R)
  R = sqrt(diag_k);
else
  col_k = x'*X; % elements of column k in X'X matrix
  R_k = R'\col_k'; % R'R_k = (X'X)_k, solve for R_k
  R_kk = sqrt(diag_k - R_k'*R_k); % norm(x'x) = norm(R'*R), find last element by exclusion
  R = [R R_k; [zeros(1,size(R,2)) R_kk]]; % update R
end






