function[out] = plars_gram_downc(A, b, options_general, options_specific)

% Positive lasso modification of the LARS (Efron et al., 2004, Section 3.4) 
% based on code of Karl Skoglund, IMM, DTU, kas@imm.dtu.dk

if options_general.mode
    AtA = A;
    Atb = b;
    c = Atb;
end
%%% Initialize objective -- global [possibly unused]
global f;
if options_general.mode 
f = @(x) x' * (AtA * x - 2 * b);  
else
    f = @(x) norm(A * x - b)^2;
end
%%% Initialize CPU time
t0 = tic;
%%%
[n p] = size(A);

%%% un-pack Gram matrix if available; otherwise, compute Gram matrix.
if ~options_general.mode
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
        AtA = A'*A;
    end
end
%%%

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
else
    mv = 1;
end
%%%

%initialize variables
nvars = min(n, p);
maxk = options_specific.itmax; % Maximum number of iterations
maxvar = options_specific.maxvar; % early stopping
I = 1:p; % inactive set
P = []; % positive set
% we shall always use the Cholesky factorization
R = []; % Cholesky factorization R'R = X'X where R is upper triangular


lassocond = 0; % LASSO condition boolean
earlystopcond = 0; % Early stopping condition boolean
k = 0; % Iteration count
vars = 0; % Current number of variables
x = zeros(p, 1);
if ~options_general.mode 
    Atb = A' * b;
    c = Atb;
end

% grad = -w_
perf = getL(options_general.perfmeas, -c, x);
check = check_termination(t0, options_general, perf, -c, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

% check 
if check.stop
   out.xopt = x; 
   out.err = f(x);
   out.ConvSpeed = ConvSpeed; 
   out.check = check;
end


%%% LARS main loop
while vars < nvars && ~earlystopcond && k < maxk && (~check.stop)
  k = k + 1;
  [C j] = max(c(I));
  if C < eps
     out.xopt = x; 
     out.err = f(x);
     out.ConvSpeed = ConvSpeed; 
     out.check = check; 
     return; 
  end
  j = I(j); % add one variable at a time only.

  if ~lassocond % if a variable has been dropped, do one iteration with this configuration (don't add new one right away)
    R = cholinsertgram(R, AtA(j, j), AtA(j,P));
    P = [P j];
    I(I == j) = [];
    vars = vars + 1;
  end

    s = ones(vars, 1);
    
    
    GA1 = R\(R'\s);
    AA = 1/sqrt(sum(GA1));
    w = AA*GA1;
  
    if mv == 2
        u = A(:,P)*w; % equiangular direction (unit vector)
        a = A'*u; % correlation between each variable and equiangular vector
    else
        a = AtA(:,P) * w; 
    end    
  
    if  vars == nvars % if all variables active, go all the way to the lsq solution
        gamma = C/AA;
    else
        temp = (C - c(I))./(AA - a(I)); % note: only positive correlations matter.
        gamma = min([temp(temp > eps); C/AA]);
    end

  % LASSO modification
 
    lassocond = 0;
    temp = (-x(P)./w)';
    %findtemp = find(temp > 0);
    [gamma_tilde] = min([temp(temp > 0) gamma]);
    j = find(abs(temp - gamma_tilde) < eps);
    if gamma_tilde < gamma
      gamma = gamma_tilde;
      lassocond = 1;
    end
  
    x(P) = x(P) + gamma*w;
  
  
  % If LASSO condition satisfied, drop variable from active set
  if lassocond == 1
      lj = length(j);
      for jj=1:lj
        R = choldown(R,j(lj - jj + 1));
        I = [I P(j)];
      end
      P(j) = [];
      vars = vars - length(j); % note that in general one may have several drops at a time.
  end
 
  % Early stopping at specified number of variables
  if maxvar > 0
    earlystopcond = vars >= maxvar;
  end
  
 if mv == 2
     c = A'* (b - A(:,P) * x(P));
  else   
     c = Atb - AtA(:,P) * x(P);   
  end
  if all(isfinite(x))
      
      perf = getL(options_general.perfmeas, -c, x);
      check = check_termination(t0, options_general, perf, -c, x, check.fprev, check.xprev);
      perf = perf(~isnan(perf));
      ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
      
  else
      break;
  end
end

out.xopt = x; 
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;


if k == maxk
  disp('LARS warning: Forced exit. Maximum number of iteration reached.');
end


%%% same function for the case that the Gram matrix has already been pre-computed.
function R = cholinsertgram(R, diag_k, col_k)
if isempty(R)
  R = sqrt(diag_k);
else
%  col_k = x'*X; % 
  R_k = R'\col_k'; % R'R_k = (X'X)_k, solve for R_k
  R_kk = sqrt(diag_k - R_k'*R_k); % norm(x'x) = norm(R'*R), find last element by exclusion
  R = [R R_k; [zeros(1,size(R,2)) R_kk]]; % update R
end









