% Name of the NNLS solver:
% Subspace Barzilai-Borwein (SBB) method. 
% 
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_sbb'.
%
%

function [out] = sbb(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

if options_general.mode
    AtA = A;
    Atb = b;
    mv = 1;
end

%%% Initialize CPU time
t0 = tic;
%%%
[n, p] = size(A);

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end

if ~options_general.mode
    %%% un-pack Gram matrix if available; otherwise, compute Gram matrix.
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
        if  mv == 1
            AtA = A'*A;
        end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% Initialize gradient

if mv == 1
   grad =@(x) (AtA * x - Atb); 
else
   grad =@(x) A' * (A * x - b); 
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) x' * (AtA * x) - 2 * x' * Atb; 
     else
        f = @(x) norm(A * x - b)^2;
     end
end
%%%
proj = @(z) max(0, z); %%% projection on positive orthant
% parameters of the method
% number of steps after which descent is checked
M = options_specific.M; 
% diminishing stepsize parameter
eta = options_specific.eta;
% parameter for assessing decrease in objective
sigma = 0.25;
% initial stepsize
beta = 1;
%%%
gradf = -Atb;
gradfold = gradf;
x = zeros(p, 1);
%%%
fref = f(x);
xref = x;
gradref = gradf;
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;
%%%
counter = M;
variant = 1; % which of the two bb-stepsize variants to use

while ~check.stop && beta > eps
    % compute complement of binding set
    B = ((gradf > 0) & (x == 0));
    % set to 0
    gradfold(B) = 0;
    % compute BB stepsize
    if  mv == 1
        part = AtA * gradfold;
    else
        part = A' * (A * gradfold);
    end
    if variant == 1
        alpha = gradfold' * part / norm(part)^2;
        variant = 2;
    else
        alpha = norm(gradfold)^2 / (gradfold' * part);
        variant = 1;
    end
     
    x = proj(x - beta * alpha * gradf);
    gradfold = gradf;
    gradf = grad(x);
    
    if counter == 0
        fcur = f(x);
        if ~(0.5 * (fref - fcur) >= sigma * (gradref' * (xref - x)))
            beta = beta * eta;
        end
        counter = M;
        fref = fcur;
        gradref = gradf;
    else
        counter = counter - 1;
    end

    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    

end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end





