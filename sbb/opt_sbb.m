% Function to set-up options specific to the
% subspace Barzilai-Borwein method (SBB).
%
% Inputs              
%
% 'mvobj'  --- scheme to be used for comparison of objective values
%              	'1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              	'2': f(x) = norm(A * x).^2 (default).
%
% 'eta'    --- Parameter between 0 and 1 (default = 0.99) controlling the dimininishment   
%              of the step-size. At each diminishment, the reduction is 
%              by a factor of 'eta'; upon initialization, the step-size is equal  
%              to one.
%
% 'M'      --- At every M-th iteration, it is decided, whether the step-size is 
%              diminished or not based on a non-monotonic Armijo condition.
%              default: 'M = 10'.
%
%

function options = opt_sbb(varargin)

p = inputParser;

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('mvobj', 2, validmvobj);

valideta = @(x)validateattributes(x,{'numeric'},{'scalar','positive','<',1});
p.addParamValue('eta', 0.99, valideta);

validM = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('M', 10, validM);

p.parse(varargin{:});
options = p.Results;





