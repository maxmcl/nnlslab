% Implementation of the subspace Barzilai-Borwein method(SBB) tailored to the case of A being a partial Fourier matrix, cf. Donoho and Tanner 
% 'Counting the faces of randomly-projected hypercubes and orthants, with applications' (2010), Section 4.2 
%
%
% Input
%
% p --- dimension of the solution vector (x). Must be an integer power of 2. 
%
% b --- observation vector
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_sbb'.

function [out] = sbb_f(p, b, options_general, options_specific)


%%% Initialize CPU time
t0 = tic;
%%%
n = length(b);

%%%

%%%
odds =  1:2:n;
evens = 2:2:(n-1);
m = length(odds);
%%%
imagix =  2:m;
%%%
% pre-compute constant part of the gradient
bcmp = complex(b(odds), [0; -b(evens)]);
bcmp = [bcmp; zeros(p - m, 1)];
Atb = real(ifft(bcmp) * p);
%
zeroes = zeros(p - m, 1);
ix = 1:m;
% construct the permutation for the synth function
ixx = zeros(n, 1);
ixx(odds) = 1:m;
ixx(evens) = (m+1):n;
%
truncate = @(x) x(ix);
getlow = @(x) truncate(fft(x));
put_real_imag = @(x) x(ixx);
synth = @(x) put_real_imag( vertcat( real(x), -imag(x(imagix)) ) );

grad = @(x)  real(ifft( vertcat( getlow(x), zeroes) ) * p) - Atb;

%%% Initialize objective
global f;
f = @(x) norm(b - synth(getlow(x)))^2;

%%%
proj = @(z) max(0, z); %%% projection on positive orthant
% parameters of the method
% number of steps after which descent is checked
M = options_specific.M; %%% default: M.
% diminishing stepsize parameter
eta = options_specific.eta; %%% default: 0.99.
% parameter for assessing decrease in objective
sigma = 0.25;
% initial stepsize
beta = 1;
%%%
gradf = -Atb;
gradfold = gradf;
x = zeros(p, 1);
%%%
fref = f(x);
xref = x;
gradref = gradf;
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

%%%
counter = M;
variant = 1; % which of the two bb-stepsize variants to use

while ~check.stop && beta > eps
    % compute complement of binding set
    B = ((gradf > 0) & (x == 0));
    % set to 0
    gradfold(B) = 0;
    % compute BB stepsize
    
    part = real(ifft( vertcat( getlow(gradfold), zeroes) ) * p);
           
    if variant == 1
        alpha = gradfold' * part / norm(part)^2;
        variant = 2;
    else
        alpha = norm(gradfold)^2 / (gradfold' * part);
        variant = 1;
    end
     
    x = proj(x - beta * alpha * gradf);
    gradfold = gradf;
    gradf = grad(x);
    
    if counter == 0
        fcur = f(x);
        if ~(0.5 * (fref - fcur) >= sigma * (gradref' * (xref - x)))
            beta = beta * eta;
        end
        counter = M;
        fref = fcur;
        gradref = gradf;
    else
        counter = counter - 1;
    end

    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    

end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end





