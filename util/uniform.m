function A = uniform(n,p,varargin)
%
% Generate a random matrix A in R^(n,p) with i.i.d. distribution of
% [0,b], where b = sqrt(3) is chosen such that the squared norms   
% of the columns are equal to 1 in expectation.

psr = inputParser;

isposint = @(x)validateattributes(x, {'numeric'},{'scalar', 'integer', 'positive'});
psr.addRequired('n', isposint);
psr.addRequired('p', isposint);

validdensity = @(x)validateattributes(x, {'numeric'},{'scalar', 'nonnegative','<=',1});
psr.addParamValue('density',1,validdensity);

psr.parse(n,p,varargin{:});
density = psr.Results.density;

if density == 1
    A = sqrt(3/n) * rand(n,p);
else
    A = sqrt(3/n/density) * sprand(n,p,density);
end

end

