function A = bernoulli(n, p, varargin)
% 
% Generate a random binary matrix A in R^(n,p), the
% entries of which are drawn i.i.d. from a Bernoulli
% distribution with parameter 'prob', which is  
% equal to the probability that A(i,j) = 1 (default: 'prob = 0.5').
% 
% optional inputs:
% 
% 'prob': a value in (0,1),
% 'sparsem': boolean. 
%  If 'sparsem = true', A is stored as a sparse matrix.
%

psr = inputParser;

isposint = @(x)validateattributes(x, {'numeric'},{'scalar', 'integer', 'positive'});
psr.addRequired('n', isposint);
psr.addRequired('p', isposint);

validdensity = @(x) validateattributes(x, {'numeric'},{'scalar', 'nonnegative','<=',1});
psr.addParamValue('prob', 0.5, validdensity);

validsparse = @(x) validateattributes(x, {'logical'}, {'scalar'});
psr.addParamValue('sparse', false, validsparse);


psr.parse(n,p,varargin{:});
prob = psr.Results.prob;
sparsem = psr.Results.sparse;

if sparsem
    A =  sparse(1/sqrt(n * prob) * (rand(n, p) > (1 - prob))); 
else
    A =  1/sqrt(n * prob) * (rand(n, p) > (1 - prob));
end

end

