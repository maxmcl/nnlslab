function  A  = gaussian( n, p, varargin )
%
%   Generate a random Gaussian matrix A in R^(n,p),
%   the rows of row which are drawn independently from a Gaussian distribution with zero
%   mean and given covariance matrix. 
%   The columns are scaled so that their squared norms are equal to 1
%   in expectation. 
%
%   optional inputs:
%
%     'padone':  If true, add ones at the top of A, output in R^(n+1,p),
%               default: false. 
%     'Sigma':   - covariance matrix in R^(p,p). default: the identity
%           
%

psr = inputParser;

isposint = @(x)validateattributes(x, {'numeric'},{'scalar', 'integer', 'positive'});
psr.addRequired('n', isposint);
psr.addRequired('p', isposint);

psr.addParamValue('padone',false,@islogical);

validSigma = @(x)validateattributes(x, {'numeric'},{'size', [p,p]});
psr.addParamValue('Sigma',[], validSigma);

validdensity = @(x)validateattributes(x, {'numeric'},{'scalar', 'nonnegative','<=',1});
psr.addParamValue('density',1,validdensity);

psr.parse(n,p,varargin{:});
options = psr.Results;

if options.density < 1 && ~isempty(options.Sigma)
    error('Must use identity matrix(default) for Sigma to generate sparse random matrix');
end

if options.padone % add ones vector at the top of matrix
    if isempty(options.Sigma) % isotropic Gaussian, Sigma = identity matrix
        if options.density == 1
            A = 1/sqrt(2*n) * randn(n+1,p);
            A(1,:) = 1/sqrt(2) * ones(p,1);
        else 
            A = 1/sqrt(2*n*options.density) * randn(n+1,p);
            A(1,:) = 1/sqrt(2) * ones(p,1);
        end
    else % anisotropic Gaussian
        A = 1/sqrt(2*n) * mvnrnd(zeros(1,p),options.Sigma,n+1);
        A(1,:) = 1/sqrt(2) * ones(p,1);
    end
else 
    if isempty(options.Sigma) 
        if options.density == 1
            A = 1/sqrt(n) * randn(n,p);
        else
            A = 1/sqrt(n * options.density) * sprandn(n,p,options.density);
        end
    else 
        A = 1/sqrt(n) * mvnrnd(zeros(1,p),options.Sigma,n);
    end
end


