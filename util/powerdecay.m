function  A  = powerdecay(n, p, varargin)
%
% Generate a random Gaussian matrix A in R^(n,p),
% the rows of which are drawn independently from a Gaussian distribution with zero
% mean and covariance matrix Sigma having entries
%
% Sigma(j,k) = rho^|j-k|,
% 
% where 0 < rho < 1 is a parameter to be set by the user.
%
% Implementation is based on the Cholesky factorization of the precision  
% matrix as described e.g. in 
% 
% 'Gaussian Markov Random Fields: Theory and Applications'
%  by H. Rue and L. Held, Chapman & Hall, 2005.
%
% optional inputs:
% 
% 'rho': a value in [0,1). 
%

psr = inputParser;

isposint = @(x) validateattributes(x, {'numeric'},{'scalar', 'integer', 'positive'});

validrho = @(x) validateattributes(x, {'numeric'},{'scalar', 'nonnegative','<=',1});
psr.addParamValue('rho', 0, validrho); % default for rho is zero

psr.addRequired('n', isposint);
psr.addRequired('p', isposint);

psr.parse(n, p, varargin{:});
rho = psr.Results.rho;

% generate precision matrix as a sparse matrix
ii = 1:p; 
bw = 3; % bandwidth = 3
jj = reshape([ii - 1; ii; ii + 1], p * bw, 1); 
jj = jj(2:(end-1));
ii = reshape(repmat(1:p, bw, 1), p * bw, 1);
ii = ii(2:(end-1));
entries = [1; -rho; repmat([-rho; (1 + rho^2); -rho], (p - 2),1); -rho; 1] ./ (1 - rho^2);
P = sparse(ii, jj, entries, p, p);
% generate samples
% step 1: compute Cholesky root
L = chol(P, 'lower');
Atilde = randn(n, p);
% step 2: solve
A = 1/sqrt(n) * transpose(L' \ Atilde');
end

