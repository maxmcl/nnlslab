function options = initopt_general(varargin)
%
% Function to set-up a series of options regarding operation
% and termination of the various NNLS solvers.
% The function is called prior to calling one of the solvers,
% and the output (a structure with names equal to those of the inputs listed below) 
% is then passed as argument 'options_general'.   
%
%
% Input
%
%   'perfmeas'      - performance measure(s) to be tracked while the solver progresses
%		      Specification is via a cell of length at most 3,
%		      the elements of which can take either one of the two values {1,2}
%                     or a non-negative vector that serves as 'ground truth' x^* of the
%                     NNLS problem. In case that such a vector is provided, norm(x^k - x^*)
%                     is tracked. The values {1,2} encode the following two
%                     performance measures:         
%			1: KKT optimality
%			2: function value f(x^k).
%		      default: {1}, i.e. only KKT optimality is tracked.  
%				 	
%                     
%   'maxcpu'        - maximum cpu time (in seconds). 
%  			The solver terminates after the iteration during which 'maxcpu'
%                       is exceeded first. Time is measured using MATLAB's 'tic' and 'toc'.
%			default: inf, i.e. the solver runs until all of the
%			stopping criteria are fulfilled (see below).	
% 
%    'stopcrit'      - stopping criteria. A vector of length at most 3, the elements
%                      of which take values in the set {1,2,3}.
%			1: KKT optimality
%			2: f(x^k) - f(x^(k+1)), i.e. convergence of the function values 
%			3: norm(x^(k+1) - x^k)/(0.5 * norm((x^(k+1) + x^k))),i.e. convergence of the iterates.
%		      If multiple stopping criteria are specified, _all_ of them have to be
%		      satisfied upon termination of the algorithm.						  
%
%    'tol'          - Numerical tolerances for the stopping criteria as specified in 'stopcrit'.
%                     Must be a vector of the same length as 'stopcrit'. default: [1E-10]    
%		     		
%
%   'usegram'       - whether to compute ('usegram' = 1) or not to compute ('usegram' = 0, default) the Gram matrix A' * A
%
%   'gram'          - precomputed Gram matrix to be used by the solver. default: empty matrix [].
%
%   'mv'            - How to perform the matrix-vector multiplications for gradient evaluation
%		      0: 'mv' is determined automatically based on the dimensions of A.
% 		      1: the gradient is evaluated as (A' * A) * x - A' * b [favourable if A is a tall matrix]
%		      2: the gradient is evaluated as A' * (b - A * x) [favourable if A is a fat matrix]
%		       	
%					 			
%                       
%   'mode'          - 0: The NNLS problem with inputs A and b is solved (default).
%                   - 1: The corresponding linear complementarity problem is solved, 
%                        in which the arguments (A,b) passed to the solver are interpreted
%	                 as follows: 
%                        A ~ A' * A,
%                        b ~ A' * b.
%                      	 Note that the specification 'mode = 1' only makes sense if 'usegram=1'.

psr =  inputParser;

validPerfmeas = @(x)iscell(x) && ~isempty(x) && length(x)<=3;
psr.addParamValue('perfmeas', {1}, validPerfmeas); 

validMaxcpu = @(x)validateattributes(x,{'numeric'},{'scalar','positive'});
psr.addParamValue('maxcpu',inf,validMaxcpu);

validStopcrit = @(x)validateattributes(x,{'numeric'},{'vector','positive','integer','<=',3});
psr.addParamValue('stopcrit',1,validStopcrit);

validTol = @(x)validateattributes(x,{'numeric'},{'vector','positive'});
psr.addParamValue('tol',10^-10,validTol);

validUsegram = @(x)validateattributes(x,{'numeric'},{'scalar','binary'});
psr.addParamValue('usegram',0,validUsegram);

validGram = @(x)validateattributes(x,{'numeric'},{'2d'});
psr.addParamValue('gram',[],validGram);

validMv = @(x)validateattributes(x,{'numeric'},{'scalar','integer','nonnegative','<=',2});
psr.addParamValue('mv',0,validMv);

validMode = @(x)validateattributes(x,{'numeric'},{'scalar','integer','nonnegative','<=',1});
psr.addParamValue('mode',0,validMode);


% Parse and validate all input arguments.
psr.parse(varargin{:});
options = psr.Results;

options.stopcrit = unique(options.stopcrit);
if length(options.tol)~= length(options.stopcrit)
    error('The length of the vector of tolerances should be equal to the number of stopping criteria.');
end

if ~isempty(options.gram)
    options.usegram = true;
end

if  options.usegram == 0
    
    
    if  options.mode == 1
        display('mode 1: automatically set usegram = 1.')    
        options.usegram = 1;
    end
    
    if options.mv == 1
        display('mv 1: automatically set usegram = 1.')
        options.usegram = 1;
    end
    
end

% example
% initopt_general()
% initopt_general('usegram',true)
% initopt_general('mv',2,'tol',10^-8);
