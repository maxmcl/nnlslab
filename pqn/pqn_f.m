% Implementation of projected quasi-Newton tailored to the case of A being a partial Fourier matrix, cf. Donoho and Tanner 
% 'Counting the faces of randomly-projected hypercubes and orthants, with applications' (2010), Section 4.2 
%
%
% Input
%
% p --- dimension of the solution vector (x). Must be an integer power of 2. 
%
% b --- observation vector
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_pqn'.

function [out] = pqn_f(p, b, options_general, options_specific)

%%% Initialize CPU time    
t0 = tic;
%%%
n = length(b);

%%%
odds =  1:2:n;
evens = 2:2:(n-1);
m = length(odds);
%%%
imagix =  2:m;
%%%
% pre-compute constant part of the gradient
bcmp = complex(b(odds), [0; -b(evens)]);
bcmp = [bcmp; zeros(p - m, 1)];
Atb = real(ifft(bcmp) * p);
%
zeroes = zeros(p - m, 1);
ix = 1:m;
% construct the permutation for the synth function
ixx = zeros(n, 1);
ixx(odds) = 1:m;
ixx(evens) = (m+1):n;
%
truncate = @(x) x(ix);
getlow = @(x) truncate(fft(x));
put_real_imag = @(x) x(ixx);
synth = @(x) put_real_imag( vertcat( real(x), -imag(x(imagix)) ) );

grad = @(x)  real(ifft( vertcat( getlow(x), zeroes) ) * p) - Atb;


%%% Initialize objective
global f;
f = @(x) norm(b - synth(getlow(x)))^2;

%%%
proj = @(z) max(0, z); %%% projection on positive orthant
K = options_specific.bfgs; %%% previous default value: 50.
% use L-BFGS two loop recursion (Nocedal and Wright, p. 225)
U = [];
W = [];
Rho = [];
%%%
gradf = -Atb;
x = zeros(p, 1);
%%%

% 
stepsel = options_specific.stepsel;
if ismember(stepsel, [1 2])
    objcur = f(x);
    s = 1/2;
    tau = 1/4;
end

%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];

%%%
FLAG = 0;
outeriter = 0;

while ~check.stop && ~FLAG     

    outeriter = outeriter + 1;
    
    fixedset = find( x < eps & gradf > eps);
    freeset = setdiff(1 : p, fixedset);
    

     descent = gradf;
     descent(fixedset) = 0;

    if  outeriter > 1
        Kbar = min(outeriter, K + 1);
        k = size(U, 2);
        alphavec = zeros(k, 1);
        for l=1:k
            alpha = Rho(Kbar - l) * dot(U(:, Kbar - l), descent);
            alphavec(l) = alpha; % used in the second loop
            descent = descent - alpha * W(:, Kbar - l);
        end
        descent = descent / (Rho(Kbar - 1) * norm( W(:, Kbar - 1))^2);
        for l=k:-1:1
            beta = Rho(Kbar - l) * dot(W(:, Kbar - l), descent);
            descent = descent + U(:, Kbar - l) * (alphavec(l) - beta);
        end
    end
     
    
    descent(fixedset) = 0;
    
    %%%
    if stepsel == 1  %%% APA.
        gamma = @(bt) proj(x - bt * descent);
        m = 0;
        storedgamma = gamma(s^m);
        objnew =  f(storedgamma);
        while 0.5 * (objcur - objnew) < tau*gradf.'*(x - storedgamma) && s^m > eps
            m = m+1;
            storedgamma = gamma(s^m);
            objnew = f(storedgamma);
        end
        d = storedgamma - x;
    end
    if stepsel == 2 %%% Armijo along feasible direction
        gamma = proj(x - descent);
        d = gamma - x; %fd: feasible direction
        m = 0;
        objnew = f(x + s^m * d);
        while (0.5 * (objcur - objnew)) < tau* gradf' * (-s^m * d) && s^m > eps
            m = m+1;
            objnew = f(x + s^m * d);
        end
        d = s^m * d;
    end
%     if stepsel == 3
%         d = (y - descent);
%         d = ((d .* (d > 0)) - y);
%         if mv == 1 && options_specific.mvobj == 1
%             t = min(1, -dot(d, grady)/(d' * (subAtA * d)));
%         else
%             t = min(1, -dot(d, grady)/(norm(subA * d)^2));
%         end
%         d = t * d;
%     end
    
    if ismember(stepsel, [1 2])
        FLAG =  objcur - objnew < eps^1.5;
        objcur = objnew;     
    end

% update Quasi-Hessian.
        % delta_x
        u = x;
        u(freeset) = d(freeset);
        u(fixedset) = 0;
        
        x = x + d;
        gradfold = gradf;
        gradf = grad(x);
        w = gradf - gradfold;
        w(fixedset) = 0;
        % rho
        rho = 1/dot(u,w);
        if ~isfinite(rho)
           %i.e. dot(u,w) = 0, i.e. we have reached a stationary point
           % check performance
           perf = getL(options_general.perfmeas, gradf, x);
           check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
           perf = perf(~isnan(perf));
           ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
           
           % return
           out.xopt = x;
           out.err = f(x);
           out.ConvSpeed = ConvSpeed;
           out.check = check;
           %
           return;
        end
        if outeriter <= K
            U(:,outeriter) = u;
            W(:,outeriter) = w;
            Rho(outeriter) = rho;
        else
            U(:,1:(K-1)) = U(:,2:K);
            U(:,K) = u;
            W(:,1:(K-1)) = W(:,2:K);
            W(:,K) = w;
            Rho(1:(K-1)) = Rho(2:K);
            Rho(K) = rho;
        end
       
       
      
    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev); 
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
     
               
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end
