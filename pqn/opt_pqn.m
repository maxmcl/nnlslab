% Function to set-up options specific to
% projected quasi-Newton.
%
% Inputs
%
% 'bfgs'    --- Option regarding the use
%               of the BGFS scheme for approximating
%               the Hessian, specified as non-negative
%               integer. If bfgs >= 1, then limited
%               memory BGFS (cf. Nocedal and Wright (2006), Ch. 7.2) 
%               with a diagonal + rank k update is used where k = bfgs.
%               Otherwise, if 'bfgs = 0', full BFGS approximation is performed.
%               default: 'bfgs = 50'.                 
%  
% 'stepsel' --- Step-size selection scheme to be used for the (scaled) projected
%               gradient step. If 'stepsel = 1', the 'Armijo-along-the-projection
%               arc' rule is used. If 'stepsel = 2', the 'Armijo-along-the-feasible-direction',
%               rule is used. If 'stepsel = 3', the limited minimization rule is used.
%               See Bertsekas (1999), Ch. 2.3, for information on these step-size selection
%               schemes. default: 'stepsel = 1'.                     
%
% 'mvobj'   --- scheme to be used for comparison of objective values
%              	'1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              	'2': f(x) = norm(A * x).^2 (default).   
% 

function options = opt_pqn(varargin)

p = inputParser;

validbfgs = @(x)validateattributes(x,{'numeric'},{'scalar','integer','nonnegative'});
p.addParamValue('bfgs', 50, validbfgs);

validstepsel = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',3});
p.addParamValue('stepsel', 1, validstepsel);

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('mvobj', 2, validmvobj);

p.parse(varargin{:});
options = p.Results;





