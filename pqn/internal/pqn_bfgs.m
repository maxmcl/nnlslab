function [out] = pqn_bfgs(A, b, options_general, options_specific)
%  L-BFGS scheme

if options_general.mode
    AtA = A;
    Atb = b;
    mv = 1;
end

%%% Initialize CPU time    
t0 = tic;
%%%
[n, p] = size(A);

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end

if ~options_general.mode
    %%% un-pack Gram matrix if available; otherwise, compute Gram matrix.
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
        if  mv == 1
            AtA = A'*A;
        end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% Initialize gradient

if mv == 1
   grad =@(x) (AtA * x - Atb); 
else
   grad =@(x) A' * (A * x - b); 
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) x' * (AtA * x) - 2 * x' * Atb; 
     else
        f = @(x) norm(A * x - b)^2;
     end
end

%%%
proj = @(z) max(0, z); %%% projection on positive orthant
K = options_specific.bfgs; %%% previous default value: 50.
% use L-BFGS two loop recursion (Nocedal and Wright, p. 225)
U = [];
W = [];
Rho = [];
%%%
gradf = -Atb;
x = zeros(p, 1);
%%%

% 
stepsel = options_specific.stepsel;
if ismember(stepsel, [1 2])
    objcur = f(x);
    s = 1/2;
    tau = 1/4;
end

%%%
perf = getL(options_general.perfmeas, gradf, x);
% fprev = inf; xprev = x + inf;
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
%%%
FLAG = 0;
outeriter = 0;

while ~check.stop && ~FLAG     

    outeriter = outeriter + 1;
    
    fixedset = find( x < eps & gradf > eps);
    freeset = setdiff(1 : p, fixedset);
    
    y = x(freeset);
    grady = gradf(freeset);
  

    if mv == 1 && options_specific.mvobj == 1
        subAtA = AtA(freeset, freeset);
        obj = @(x) x' * (subAtA  * x) - 2 * x' * Atb(freeset);
    else
        subA = A(:, freeset);
        obj = @(x) norm(subA*x - b)^2;
    end
    
    if options_general.mode
       subAtA = AtA(freeset, freeset);
       obj = @(x) x' * (subAtA  * x) - 2 * x' * Atb(freeset);
    end

    descent = gradf;
    descent(fixedset) = 0;

    if  outeriter > 1
        Kbar = min(outeriter, K + 1);
        k = size(U, 2);
        alphavec = zeros(k, 1);
        for l=1:k
            alpha = Rho(Kbar - l) * dot(U(:, Kbar - l), descent);
            alphavec(l) = alpha; % used in the second loop
            descent = descent - alpha * W(:, Kbar - l);
        end
        descent = descent / (Rho(Kbar - 1) * norm( W(:, Kbar - 1))^2);
        for l=k:-1:1
            beta = Rho(Kbar - l) * dot(W(:, Kbar - l), descent);
            descent = descent + U(:, Kbar - l) * (alphavec(l) - beta);
        end
    end
     
    descent = descent(freeset);
    
    %%%
    if stepsel == 1  %%% APA.
        gamma = @(bt) proj(y - bt * descent);
        m = 0;
        storedgamma = gamma(s^m);
        objnew =  obj(storedgamma);
        while 0.5 * (objcur - objnew) < tau*grady.'*(y - storedgamma) && s^m > eps
            m = m+1;
            storedgamma = gamma(s^m);
            objnew = obj(storedgamma);
        end
        d = storedgamma - y;
    end
    if stepsel == 2 %%% Armijo along feasible direction
        gamma = proj(y - descent);
        d = gamma - y; %fd: feasible direction
        m = 0;
        objnew = obj(y + s^m * d);
        while (0.5 * (objcur - objnew)) < tau* grady' * (-s^m * d) && s^m > eps
            m = m+1;
            objnew = obj(y + s^m * d);
        end
        d = s^m * d;
    end
    if stepsel == 3
        d = (y - descent);
        d = ((d .* (d > 0)) - y);
        if mv == 1 && options_specific.mvobj == 1
            t = min(1, -dot(d, grady)/(d' * (subAtA * d)));
        else
            t = min(1, -dot(d, grady)/(norm(subA * d)^2));
        end
        d = t * d;
    end
    
    if ismember(stepsel, [1 2])
        FLAG =  objcur - objnew < eps^1.5;
        objcur = objnew;     
    end

% update Quasi-Hessian.
        % delta_x
        u = x;
        u(freeset) = d;
        u(fixedset) = 0;
        % delta_gradient
        x(freeset) = y + d;
        gradfold = gradf;
        gradf = grad(x);
        w = gradf - gradfold;
        w(fixedset) = 0;
        % rho
        rho = 1/dot(u,w);
        if ~isfinite(rho)
           %i.e. dot(u,w) = 0, i.e. we have reached a stationary point
           % check performance
           perf = getL(options_general.perfmeas, gradf, x);
           check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
           perf = perf(~isnan(perf));
           ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
           % return
           out.xopt = x;
           out.err = f(x);
           out.ConvSpeed = ConvSpeed;
           out.check = check;
           %
           return;
        end
        if outeriter <= K
            U(:,outeriter) = u;
            W(:,outeriter) = w;
            Rho(outeriter) = rho;
        else
            U(:,1:(K-1)) = U(:,2:K);
            U(:,K) = u;
            W(:,1:(K-1)) = W(:,2:K);
            W(:,K) = w;
            Rho(1:(K-1)) = Rho(2:K);
            Rho(K) = rho;
        end
       
       
      
    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);  
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [ toc(t0) perf]];
    
               
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end
