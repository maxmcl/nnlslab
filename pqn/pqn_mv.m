% Implementation of Projected Quasi-Newton 
% for fast matrix-vector multiplications. 
% Useful in conjunction with the package 
% 'Restore tools' available from http://www.mathcs.emory.edu/~nagy/RestoreTools/
%
% Input
%
% A --- Object for which A * v and A* * w for vectors
%       v,w of appropriate dimension is performed
%       by a fast routine for matrix-vector multiplication.
%       Examples: A represents a convolution, fast
%                 Wavelet transform etc.  
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_pqn'.

function [out] = pqn_mv(A, b, options_general, options_specific)
%  L-BFGS scheme

if options_general.mode
    AtA = A;
    Atb = b;
    mv = 1;
end

%%% Initialize CPU time    
t0 = tic;
%%%
[n, p] = size(A);

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end


%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% Initialize gradient


   grad =@(x) A' * (A * x - b); 


%%% Initialize objective
global f;
f = @(x) norm(A * x - b)^2;

%%%
proj = @(z) max(0, z); %%% projection on positive orthant
K = options_specific.bfgs; %%% previous default value: 50.
% use L-BFGS two loop recursion (Nocedal and Wright, p. 225)
U = [];
W = [];
Rho = [];
%%%
gradf = -Atb;
x = zeros(p, 1);
%%%

% 
stepsel = options_specific.stepsel;
if ismember(stepsel, [1 2])
    objcur = f(x);
    s = 1/2;
    tau = 1/4;
end

%%%
perf = getL(options_general.perfmeas, gradf, x);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
%
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
%%%
FLAG = 0;
outeriter = 0;

while ~check.stop && ~FLAG     

    outeriter = outeriter + 1;
    
    fixedset = find( x < eps & gradf > eps);
    freeset = setdiff(1 : p, fixedset);
    
    y = x(freeset);
    grady = gradf(freeset);
  

    descent = gradf;
    descent(fixedset) = 0;

    if  outeriter > 1
        Kbar = min(outeriter, K + 1);
        k = size(U, 2);
        alphavec = zeros(k, 1);
        for l=1:k
            alpha = Rho(Kbar - l) * dot(U(:, Kbar - l), descent);
            alphavec(l) = alpha; % used in the second loop
            descent = descent - alpha * W(:, Kbar - l);
        end
        descent = descent / (Rho(Kbar - 1) * norm( W(:, Kbar - 1))^2);
        for l=k:-1:1
            beta = Rho(Kbar - l) * dot(W(:, Kbar - l), descent);
            descent = descent + U(:, Kbar - l) * (alphavec(l) - beta);
        end
    end
     
    descent(fixedset) = 0;
    
    %%%
    if stepsel == 1  %%% APA.
        gamma = @(bt) proj(x - bt * descent);
        m = 0;
        storedgamma = gamma(s^m);
        objnew =  f(storedgamma);
        while 0.5 * (objcur - objnew) < tau*gradf.'*(x - storedgamma) && s^m > eps
            m = m+1;
            storedgamma = gamma(s^m);
            objnew = f(storedgamma);
        end
        d = storedgamma - x;
    end
    if stepsel == 2 %%% Armijo along feasible direction
        gamma = proj(x - descent);
        d = gamma - x; %fd: feasible direction
        m = 0;
        objnew = f(x + s^m * d);
        while (0.5 * (objcur - objnew)) < tau* gradf' * (-s^m * d) && s^m > eps
            m = m+1;
            objnew = f(x + s^m * d);
        end
        d = s^m * d;
    end
    
    
    u = x;
    u(freeset) = d(freeset);
    u(fixedset) = 0;
 
    x = x + d;
    gradfold = gradf;
    gradf = grad(x);
    w = gradf - gradfold;
    w(fixedset) = 0;
    % rho
    rho = 1/dot(u,w);
    if ~isfinite(rho)
        %i.e. dot(u,w) = 0, i.e. we have reached a stationary point
        % check performance
        perf = getL(options_general.perfmeas, gradf, x);
        check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
        perf = perf(~isnan(perf));
        ConvSpeed =  [ConvSpeed; [(cputime - t0) perf]];
        
        % return
        out.xopt = x;
        out.err = f(x);
        out.ConvSpeed = ConvSpeed;
        out.check = check;
        %
        return;
    end
    if outeriter <= K
        U(:,outeriter) = u;
        W(:,outeriter) = w;
        Rho(outeriter) = rho;
    else
        U(:,1:(K-1)) = U(:,2:K);
        U(:,K) = u;
        W(:,1:(K-1)) = W(:,2:K);
        W(:,K) = w;
        Rho(1:(K-1)) = Rho(2:K);
        Rho(K) = rho;
    end
    
       
       
      
    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev); 
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [ toc(t0) perf]];
     
               
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end
