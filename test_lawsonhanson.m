%% Testing FNNLS on the problem
% min_x { 1/2 * || A*x - b ||^2 : x >= 0 }
clc;
clear all;
close all;

addpath('~/Masters/fnnls');
addpath(genpath('~/Masters/nnlslab'));

diary('nnls_solvers_test.txt');

%% NNLSlab parameters -- outside for loop
optUD = opt_lawsonhanson('up', 1, 'down', 1);
optUDc = opt_lawsonhanson('up', 1, 'down', 2);
optU = opt_lawsonhanson('up', 1, 'down', 0);
opt = opt_lawsonhanson('up', 0, 'down', 0);

%% Generate problem 1/2 * || A*x - b ||^2 for several problem sizes
runTime = struct;
nrmNNLS = struct;

pairs = { [42, 6], [100, 50], [100, 100], [500, 100], [500, 500], ...
    [1000, 500], [1000, 1000], [2500, 1000], [2500, 2500]};
iter = 1;

for temp = pairs;
    fprintf('\t\t\t Run #%d\n', iter);
    m = temp{1}(1);
    n = temp{1}(2);
    A = randi(1e3, m, n) - randi(1e3, m, n);
    b = -randi(50, m, 1) - randi(50, m, 1);
    
    %% Solve using lsqnonneg
    tic;
    tol = 10 * eps * norm(A, 1) * length(A);
    optNNLS = optimset('TolX', tol);
    xRef = lsqnonneg(A, b, optNNLS);
    runTime.nnls = toc;
    nrmNNLS.nnls = 0;
    fprintf('nnls!\n');
    
    %% Solve using FNNLS
    tic;
    AtA = A'*A;
    Atb = A'*b;
    xOut = fnnls(AtA, Atb, tol);
    runTime.fnnls = toc;
    nrmNNLS.fnnls = norm(xOut - xRef);
    fprintf('fnnls!\n');
    
    %% Solve using NNLSlab
    genOptF = initopt_general('stopcrit', 1, 'usegram', 1, 'mv', 0, 'tol', ...
        tol, 'mode', 1);
    genOpt = initopt_general('stopcrit', 1, 'usegram', 0, 'mv', 0, 'tol', ...
        tol, 'mode', 0);
    lhFUD = lawsonhanson(A, b, genOptF, optUD);
    fprintf('lhfud!\n');
    lhFUDc = lawsonhanson(A, b, genOptF, optUDc);
    fprintf('lhfudc!\n');
    lhFU = lawsonhanson(A, b, genOptF, optU);
    fprintf('lhfu!\n');
    lhF = lawsonhanson(A, b, genOptF, opt);
    fprintf('lhf!\n');
    lhUD = lawsonhanson(A, b, genOpt, optUD);
    fprintf('lhud!\n');
    lhUDc = lawsonhanson(A, b, genOpt, optUDc);
    fprintf('lhudc!\n');
    lhU = lawsonhanson(A, b, genOpt, optU);
    fprintf('lhu!\n');
    lh = lawsonhanson(A, b, genOpt, opt);
    fprintf('lh!\n');
    
    names = {'lhFUD', 'lhFUDc', 'lhFU', 'lhFU', 'lhF', 'lhUD', ...
        'lhUDc', 'lhU', 'lh'};
    for name = names
        % Grabbing struct corresponding to name
        aStrct = eval(name{1});
        % Assigning values
        runTime.(name{1}) = aStrct.time;
        nrmNNLS.(name{1}) = norm(aStrct.xopt - xRef);
    end
    
    %% Parsing whole run
    fprintf('\n\n %s \n\n', repmat('-', 50));
    fprintf('m = %d, n = %d\n\n', m, n);
    fprintf('%8s\t%8s\t%8s\n', 'Solver', 'Run time', 'NrmToRef');
    for f = fields(runTime)'
       fprintf('%8s\t%8.1e\t%8.1e\n', f{1}, runTime.(f{1}), nrmNNLS.(f{1}));
    end
    fprintf('\n\n %s \n\n', repmat('-', 50));
    iter = iter + 1;
end

diary off;