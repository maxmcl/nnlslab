% Name of the NNLS solver:
% Projected gradient. 
% 
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_projgradient'.

function [out] = projgradient(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
    error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
    error('Structure of stopcrit not compatible with structure of tol')
end

if options_general.mode
    AtA = A;
    Atb = b;
    mv = 1;
end
 
 %%% Initialize CPU time
 t0 = tic;
 %%%
 [n, p] = size(A);
 
 %%%
 if ~options_general.mode
     if options_general.mv == 0
         if 1.1 * 2 * n < p
             mv = 2;
         else
             mv = 1;
         end
     else
         mv = options_general.mv;
     end
 end

 if ~options_general.mode
    
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
       if  mv == 1
           AtA = A'*A;
       end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% Initialize gradient

if mv == 1
   grad =@(x) (AtA * x - Atb); 
else
   grad =@(x) A' * (A * x - b); 
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) x' * (AtA * x) - 2 * x' * Atb; 
     else
        f = @(x) norm(A * x - b)^2;
     end
end

%%% Initialize 'restricted objectives' and gradient for line search

global fP;
if options_general.mode 
    fP = @(y, P) y' * (AtA(P,P) * y) - 2 * y' * Atb(P);  
else
    if mv == 1 && options_specific.mvobj == 1
        fP = @(y, P) y' * (AtA(P,P)  * y) - 2 * y' * Atb(P); 
    else
        fP = @(y, P) norm(A(:,P) * y - b)^2;
    end
end

%%%
stepsel = options_specific.stepsel;
global scale;
global W;
global Winv;
scale = options_specific.scale;

if scale
   %%% compute/extract diagonal entries of the Gram matrix
   if mv == 1
       W = sqrt(diag(AtA));
       Winv = 1./W;
   else
       W = sqrt(sum(A.^2));
       Winv = (1./W)';
   end
end

if stepsel == 1
    if  options_specific.L > 0;
        t = 1/options_specific.L * 0.99;
    else
        if mv == 1
            t = 1 / normest(AtA) * 0.99;
        else
            t = 1/(normest(A)^2) * 0.99;
        end
    end
    if scale
       t = (1./max(Winv)^2) * t;
    end
end
gradf = -Atb;
x = zeros(p, 1);
if ismember(stepsel, [2 3])
    objcur = f(x);
end
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
%%%

while ~check.stop
   
   d = gradf;
   if scale
      d = Winv .* d;
   end
    
   switch stepsel 
       case 1
           if scale
               x = Winv .* max(0, W .* x - t * d);
           else
               x =  max(0, x - t * d);
           end
       case 2
            [x, objcur, FLAG] = APA(x, gradf, d, objcur);
            if FLAG
                break;
            end
       case 3
            [x, objcur, FLAG] = AFD(x, gradf, d, objcur);
            if FLAG
              break;   
            end
       case 4
	    if scale
               fd = (W .* x - d);
	       fd = (Winv .* (fd .* (fd > 0)) - x);
	    else
	       fd = (x - d);
	       fd = ((fd .* (fd > 0)) - x);
	    end
            if options_general.mode
                t = min(1, -dot(fd, gradf)/(fd' * (AtA * fd)));
            else
                t = min(1, -dot(fd, gradf)/(norm(A * fd)^2));
            end
            x = x + t * fd;           
   end
   
   gradf = grad(x);
   
   perf = getL(options_general.perfmeas, gradf, x);
   check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
   perf = perf(~isnan(perf));
   ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
   
end

%%% 

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end


%%% Line search for APA rule.
function [xnew, objnew, FLAG] = APA(x, d, gradf, objcur) %%% d -- a descent direction.

global f;
global fP;
global scale;
global W;
global Winv;
%%% d
s = 1/2;
tau = 1/4;
%%%
fixedset = find(x < eps & d > eps);
freeset = setdiff(1 : length(x), fixedset);
proj = @(z) max(0, z); 
if length(freeset) < 0.2 * length(x)
    y = x(freeset);
    dy = d(freeset);
    grady = gradf(freeset); 
    %%% do a series of restricted function evaluations

    if scale  
      gamma = @(bt)  Winv .* proj(W .* y - bt * dy);
    else
      gamma = @(bt) proj(y - bt * dy);
    end
    m = 0;
    storedgamma = gamma(s^m);
    objnew = fP(storedgamma, freeset);
    while (0.5 * (objcur - objnew)) < tau*grady'*(y - storedgamma) && s^m > eps
        m = m+1;
        storedgamma = gamma(s^m);
        objnew = fP(storedgamma, freeset);
    end
    xnew = x;
    xnew(freeset) = storedgamma;
else
    %%% do a series of function evaluations
    if scale
      gamma = @(bt)  Winv .* proj(W .* x - bt *  d);
else
      gamma = @(bt) proj(x - bt *  d);
end
    m = 0;
    storedgamma = gamma(s^m);
    objnew = f(storedgamma);
    
    while (0.5 * (objcur - objnew)) < (tau*gradf'*(x - storedgamma)) && s^m > eps
        m = m+1;
        storedgamma = gamma(s^m);
        objnew = f(storedgamma);
    end
    xnew = storedgamma;
    %
end
FLAG = objcur - objnew < eps^1.5;
end
 
%%% Line Search for Armijo rule along feasible direction (AFD)
function [xnew, objnew, FLAG] = AFD(x, d, gradf, objcur) %%d: descent direction
global f;
global fP;
global scale;
global W;
global Winv;
%%% define some universal constants
s = 1/2;
tau = 1/4;
%%%
fixedset = find(x < eps & d > eps);
freeset = setdiff(1:length(x), fixedset);
proj = @(z) max(0, z); 
if length(freeset) < 0.2 * length(x)
    y = x(freeset);
    dy = d(freeset);
    grady = gradf(freeset);
    if scale
      gamma = Winv .* proj(W .* y - dy); 
    else
      gamma = proj(y - dy);
    end
    fd = gamma - y; %fd: feasible direction   
    m = 0;
    objnew =  fP(y + s^m * fd, freeset);
    while (0.5 * (objcur - objnew)) < tau* grady' * (-s^m * fd) && s^m > eps 
          m = m+1;
          objnew = fP(y + s^m * fd, freeset); 
    end   
    fd = s^m * fd;
    xnew = x;
    xnew(freeset) = x(freeset) + fd;
else
    if scale
        gamma = Winv .* proj(W .* x - d);
    else
        gamma = proj(x - d);
    end
    fd = gamma - x; %fd: feasible direction   
    m = 0;
    objnew =  f(x + s^m * fd);
    while (0.5 * (objcur - objnew)) < tau* gradf'*(-s^m * fd) && s^m > eps 
          m = m+1;
          objnew = f(x + s^m * fd); 
    end   
    fd = s^m * fd; 
    xnew = x + fd;
end
 FLAG = objcur - objnew < eps^1.5;
end



