% Implementation of projected gradient tailored to the case of A being a partial Fourier matrix, cf. Donoho and Tanner 
% 'Counting the faces of randomly-projected hypercubes and orthants, with applications' (2010), Section 4.2 
%
%
% Input
%
% p --- dimension of the solution vector (x). Must be an integer power of 2. 
%
% b --- observation vector
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_projgradient'.

function [out] = projgradient_f(p, b, options_general, options_specific)

if ~all(size(options_general.stopcrit) == size(options_general.tol))
    error('Structure of stopcrit not compatible with structure of tol')
end

%%% Initialize CPU time
t0 = tic;
%%%
n = length(b);

%%% 
odds =  1:2:n;
evens = 2:2:(n-1);
m = length(odds);
%%%
imagix =  2:m;
%%%
% pre-compute constant part of the gradient
bcmp = complex(b(odds), [0; -b(evens)]);
bcmp = [bcmp; zeros(p - m, 1)];
Atb = real(ifft(bcmp) * p);
%
zeroes = zeros(p - m, 1);
ix = 1:m;
% construct the permutation for the synth function
ixx = zeros(n, 1);
ixx(odds) = 1:m;
ixx(evens) = (m+1):n;
%
truncate = @(x) x(ix);
getlow = @(x) truncate(fft(x));
put_real_imag = @(x) x(ixx);  
synth = @(x) put_real_imag( vertcat( real(x), -imag(x(imagix)) ) );

grad = @(x)  real(ifft( vertcat( getlow(x), zeroes) ) * p) - Atb; 


%%% Initialize objective
global f;
f = @(x) norm(b - synth(getlow(x)))^2;  


%%% Initialize 'restricted objectives' and gradient for line search

stepsel = options_specific.stepsel;

gradf = -Atb;
x = zeros(p, 1);
if ismember(stepsel, [2 3])
    objcur = f(x);
end
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;
%%%

while ~check.stop
   
   d = gradf;

    
   switch stepsel 
       case 1 
            x = max(0, x - t * d);
       case 2
            [x, objcur, FLAG] = APA(x, gradf, d, objcur);
            if FLAG
                break;
            end
       case 3
            [x, objcur, FLAG] = AFD(x, gradf, d, objcur);
            if FLAG
              break;   
            end
       case 4
            fd = (x - d);
            fd = ((fd .* (fd > 0)) - x);
            
            t = min(1, -dot(fd, gradf)/(norm(mytrunc(conv(h, fd)))^2));
            
            x = x + t * fd;           
   end
   
   gradf = grad(x);
   
   perf = getL(options_general.perfmeas, gradf, x);
   check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
   perf = perf(~isnan(perf));
   ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
   
end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end


%%% Line search for APA rule.
function [xnew, objnew, FLAG] = APA(x, d, gradf, objcur) %%% d -- a descent direction.

global f;
%%% define some universal constants
s = 1/2;
tau = 1/4;
%%%

proj = @(z) max(0, z); %%% projection on positive orthant

    %%% do a series of function evaluations
    gamma = @(bt) proj(x - bt *  d);
    m = 0;
    storedgamma = gamma(s^m);
    objnew = f(storedgamma);
    
    while (0.5 * (objcur - objnew)) < (tau*gradf'*(x - storedgamma)) && s^m > eps
        m = m+1;
        storedgamma = gamma(s^m);
        objnew = f(storedgamma);
    end
    xnew = storedgamma;
    %
% end
FLAG = objcur - objnew < eps^1.5;
end
 
%%% Line Search for Armijo rule along feasible direction (AFD)
function [xnew, objnew, FLAG] = AFD(x, d, gradf, objcur) %%d: descent direction
global f;
%%% define some universal constants
s = 1/2;
tau = 1/4;
%%%
    gamma = proj(x - d);
    fd = gamma - x; %fd: feasible direction   
    m = 0;
    objnew =  f(x + s^m * fd);
    while (0.5 * (objcur - objnew)) < tau* gradf'*(-s^m * fd) && s^m > eps 
          m = m+1;
          objnew = f(x + s^m * fd); 
    end   
    fd = s^m * fd; 
    xnew = x + fd;
% end
 FLAG = objcur - objnew < eps^1.5;
end



