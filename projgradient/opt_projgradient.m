% Function to set-up options specific to
% projected gradient.
%
% Inputs
%        
%  
% 'stepsel' --- Step-size selection scheme to be used for the (scaled) projected
%               gradient step. If 'stepsel = 1', a constant step-size based on the 
%               Lipschitz constant of the gradient is used. If 'stepsel = 2', the 'Armijo-along-the-projection
%               arc' rule is used. If 'stepsel = 3', the 'Armijo-along-the-feasible-direction',
%               rule is used. If 'stepsel = 4', the limited minimization rule is used.
%               See Bertsekas (1999), Ch. 2.3, for information on these step-size selection
%               schemes. default: 'stepsel = 1'.                     
%
% 'mvobj'  --- scheme to be used for comparison of objective values
%              	'1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              	'2': f(x) = norm(A * x).^2 (default).
%
% 'L'      ---  An upper bound on the Lipschitz constant of the gradient.
%               Default is '0', in which case 'normest' is called by
%              'projgradient' to upper bound the Lipschitz constant of the gradient. 
%
%
% 'scale'  --- If 'scale = 1', gradient scaling based on the diagonal entries 
%              of the Hessian is applied (cf. Bertsekas (1999), Ch. 2.3).
%              If 'scale = 0' (default), no such scaling is used.
%
%    

function options = opt_projgradient(varargin)

p = inputParser;

validstepsel = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=', 4});
p.addParamValue('stepsel', 1, validstepsel);

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=', 2});
p.addParamValue('mvobj', 2, validmvobj);

validL = @(x)validateattributes(x,{'numeric'},{'scalar','nonnegative'});
p.addParamValue('L', 0, validL);

validscale = @(x)validateattributes(x,{'numeric'},{'scalar','binary'});
p.addParamValue('scale',0,validscale);

p.parse(varargin{:});
options = p.Results;





