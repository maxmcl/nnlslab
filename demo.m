%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  demo code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% modify according to your installation directory
cd installationdir
addpath(genpath(pwd))

%%%*********************************************************************%%%
%%% EXAMPLE 1
%%%*********************************************************************%%%

% generate random input dataset I

A = randn(1500, 500);
x0 = sprand(500, 1, 0.1);
b = A * x0 + 0.003 * randn(1500, 1);

% Start by setting up general options
opt_gen1 = initopt_general('perfmeas', {1,2}, 'maxcpu', 5, 'stopcrit', 2, 'tol', 1E-12);

% Run projected gradient
pg_1 = projgradient(A, b, opt_gen1, opt_projgradient());
pg_2 = projgradient(A, b, opt_gen1, opt_projgradient('stepsel', 2));
pg_4 = projgradient(A, b, opt_gen1, opt_projgradient('stepsel', 4));

% Visualize performance
figure; hold on; xlabel('log10(CPU time)'); ylabel('log10(objective)')
plot(log10(pg_1.ConvSpeed(:,1)), log10(pg_1.ConvSpeed(:,3)), '-*')
plot(log10(pg_2.ConvSpeed(:,1)), log10(pg_2.ConvSpeed(:,3)),'-o','color','k')
plot(log10(pg_4.ConvSpeed(:,1)), log10(pg_4.ConvSpeed(:,3)),'-x','color','r')
legend('fixed', 'APA', 'limited minimization', 'APA, optimized evaluation of objective') 
% 

% Check for improvement
pg_2b = projgradient(A, b, opt_gen1, opt_projgradient('mvobj', 1, 'stepsel', 2));
plot(log10(pg_2b.ConvSpeed(:,1)), log10(pg_2b.ConvSpeed(:,3) + norm(b).^2) ,'-.o','color','k')


%*************************************************************************%
%%% EXAMPLE 2: large margin linear separation
%*************************************************************************%


load largemargin.mat
figure; hold on; xlim([-1,1]); ylim([-1,1])
plot(X(y==1,1),X(y==1,2),'x','color','black','MarkerSize',10)
plot(X(y==-1,1),X(y==-1,2),'o','color','black','MarkerSize',10)

G = diag(y) * (X * X') * diag(y);
opt_lcp = initopt_general('mode', 1);
sol = mupdates(G, ones(size(X, 1), 1), opt_lcp, opt_mupdates());

w = X' * (sol.xopt .* y);
wperp = [w(2); -w(1)]; slope = wperp(2)/wperp(1);
plot([-1;1], slope * [-1,1], 'LineWidth', 3)

%%%******************* End of demon.m **********************************%%%


