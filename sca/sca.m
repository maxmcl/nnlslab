% Name of the NNLS solver:
% Sequential coordinate-wise algorithm (SCA): 
% a coordinate descent algorithm for the NNLS problem.
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- No specific options need to be provided, i.e. theat argument can be omitted. 
%                             

function [out] = sca(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

%%%
usegram = options_general.usegram;
%%%
if options_general.mode
     AtA = A;
     AtAdiag = diag(AtA);
     Atb = b;
     usegram = 1;
 end
 
 %%% Initialize CPU time
 t0 = tic;
 %%%
 [n, p] = size(A); 
 if ~options_general.mode
    %%% un-pack Gram matrix if available; otherwise, compute Gram matrix. 
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        AtAdiag = diag(AtA);
        options_general.gram = [];
    else
       if  usegram == 1
           AtA = A'*A;
           AtAdiag = diag(AtA);
       else
           At = A';
           AtAdiag = sum(A.^2);
       end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end
%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
    f = @(x) norm(A * x - b)^2;
end
%%%
gradf = -Atb;
x = zeros(p, 1);
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];

%%%

while ~check.stop
     for i = 1:p 
          xi = x(i); 
          si = gradf(i);
          if ~(xi == 0 && si >= 0) 
            t = max(0, xi - si/AtAdiag(i));
            if usegram 
               gradf = gradf + (t - x(i)) * AtA(:,i);
            else
               gradf = gradf + (t - x(i)) * (At * A(:,i)); 
            end
            x(i) = t;
          end 
     end
     
     perf = getL(options_general.perfmeas, gradf, x);
     check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
     perf = perf(~isnan(perf));
     ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
     
     
end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end
