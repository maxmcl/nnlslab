% Name of the NNLS solver:
% Multiplicative updates.
%
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_mupdates'.

function [out] = mupdates(A, b, options_general, options_specific)

  
if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

%%%

t0 = tic;

%%%

p = size(A, 2);

%%%
onlypos = options_specific.onlypos;
%%% 
if options_general.mode
    if ~onlypos
        if issparse(A)
            [ii, jj, val] = find(A);
            boolpos = val >= 0;
            if sum(boolpos) == length(boolpos)
                onlypos = 1;
            else
                Aplus = sparse(ii(boolpos), jj(boolpos), val(boolpos), p, p);
                Aminus = sparse(ii(~boolpos), jj(~boolpos), -val(~boolpos), p, p);
            end
        else
            boolpos = A >= 0;
            if sum(boolpos) == length(boolpos)
                onlypos = 1;
            else
                Aplus = A;
                Aplus(~boolpos) = 0;
                Aminus = -A;
                Aminus(boolpos) = 0;
            end
        end
    end
else
    bb = b; % back-up
    b = A' * b;
    if ~isempty(options_general.gram)
        if(onlypos)
            A = options_general.gram;
        else
            if issparse(options_general.gram)
                [ii, jj, val] = find(options_general.gram);
                boolpos = val >= 0;
                if sum(boolpos) == length(boolpos)
                    A = options_general.gram;
                    onlypos = 1;
                else
                    Aplus = sparse(ii(boolpos), jj(boolpos), val(boolpos), p, p);
                    Aminus = sparse(ii(~boolpos), jj(~boolpos), -val(~boolpos), p, p);
                end
            else
                boolpos = options_general.gram >= 0;
                if sum(boolpos) == length(boolpos)
                    onlypos = 1;
                else
                    Aplus = options_general.gram;
                    Aplus(~boolpos) = 0;
                    Aminus = -options_general.gram;
                    Aminus(boolpos) = 0;
                end
            end
        end
    else
        %%%
        if options_general.usegram
            A = A' * A;
            %%%
            if ~onlypos
                if issparse(A)
                    [ii, jj, val] = find(A);
                    boolpos = val >= 0;
                    if sum(boolpos) == length(boolpos)
                        onlypos = 1;
                    else
                        Aplus = sparse(ii(boolpos), jj(boolpos), val(boolpos), p, p);
                        Aminus = sparse(ii(~boolpos), jj(~boolpos), -val(~boolpos), p, p);
                    end
                else
                    boolpos = A >= 0;
                    if sum(boolpos) == length(boolpos)
                        onlypos = 1;
                    else
                        Aplus = A;
                        Aplus(~boolpos) = 0;
                        Aminus = -A;
                        Aminus(boolpos) = 0;
                    end
                end
            end
        else % we do not store the Gram matrix and only work 
             % with matrix-vector multiplications !
             if ~onlypos
                if issparse(A)
                   if sum(sum(A > eps)) == nnz(A)
                      onlypos = 1; 
                   end
                else
                    if all(A >= 0)
                        onlypos = 1;
                    end
                end
                if ~onlypos
                    error('error for mupdates: "usegram" false, but A not non-negative')
                end
             end
        end
    end
end


%%% starting value
x = ones(p, 1);
%%%

%%%
%%% 
global f;
if options_general.mode 
    f = @(x) x' * (A * x) - 2 * x' * b;
else
    if options_general.usegram
        f = @(x) x' * (A * x) - 2 * x' * b + norm(bb)^2;
    else
        f = @(x) norm(bb - A * x)^2;
    end
end

%%% backup
Atb = b;
%%% 
% Re-define A,b; introduce y = x(active)
if onlypos
     active = b > eps;
     x(~active) = 0;
     y = x(active);
     b = b(active);
     if options_general.mode || options_general.usegram
         A = A(active, active);
     else
         A = A(:,active);
     end
else
    active = true(p, 1);
    y = x;
end 

%%%
if options_general.mode || options_general.usegram
    if onlypos
        a  = A * y;
        gradf =  a - b;
    else
        aplus = Aplus * y;
        aminus = Aminus * y;
        gradf = aplus  - aminus - b;
    end
else
    % we assume that 'onlypos = 1'  here 
    a  = A' * (A * y);
    gradf =  a - b;    
end

%%% augment the gradient
gradfull = zeros(p, 1);
gradfull(~active) = -Atb(~active);
gradfull(active) = gradf;
%%%
perf = getL(options_general.perfmeas, gradfull, x);
check = check_termination(t0, options_general, perf, gradfull, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];


%%%
while ~check.stop
    
     %%%    
     if onlypos 
        
         y =  (b ./ a) .* y; 
     
     else
                 
         y =  ((b + sqrt(b.^2 + 4 * aplus .* aminus))./(2 * aplus)) .* y;
      
         rmvix = y < eps | isnan(y);
         
         flag = any(rmvix);
         
         if flag
            activeix = find(active);
            activeix(rmvix) = [];
            active = false(p, 1);
            active(activeix) = true;
            y = y(~rmvix);
            x(~active) = 0;
         end
         
     end
       
     if options_general.mode || options_general.usegram
         if onlypos
             a  = A * y;
             gradf =  a - b;
         else
             if flag
                 Aplus = Aplus(~rmvix, ~rmvix);
                 Aminus = Aminus(~rmvix, ~rmvix);
                 b = b(~rmvix);
                 gradfull(~active) = -Atb(~active);
                 flag = 0;
             end
             aplus = Aplus * y;
             aminus = Aminus * y;
             gradf = aplus  - aminus - b;
         end
     else
         % we assume that 'onlypos = 1'  here
         a  = A' * (A * y);
         gradf =  a - b;
     end
     
     gradfull(active) = gradf;
     x(active) = y;

     perf = getL(options_general.perfmeas, gradfull, x);
     check = check_termination(t0, options_general, perf, gradfull, x, check.fprev, check.xprev);
     perf = perf(~isnan(perf));
     ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
     
     
end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end
