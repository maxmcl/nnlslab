% Function to set-up options specific to
% the multiplicative updates method.
%
% Inputs
%
% 'onlypos' --- If 'onlypos = 1', the multiplicative updates method
%               is run under the assumption that the Gram matrix
%               A' * A is entry-wise non-negative, which will
%               lead to a significant simplication/speed-up
%               (note that this implied by the fact that A is entry-wise non-negative).
%               default: 'onlypos = 0', i.e. it is _not_ 
%                assumed that A' * A is entry-wise non-negative).   

function options = opt_mupdates(varargin)

p = inputParser;


validonlypos = @(x)validateattributes(x,{'numeric'},{'scalar','binary'});
p.addParamValue('onlypos', 0, validonlypos);

p.parse(varargin{:});
options = p.Results;



