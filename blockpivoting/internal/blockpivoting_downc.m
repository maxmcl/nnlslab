% *internal code not to be called directly by the user*  

 
function[out] = blockpivoting_downc(A, b, options_general, options_specific)

if options_general.mode
   AtA = A;
   Atb = b;
end

%%% Initialize CPU time
t0 = tic;
%%%

if  options_specific.qbar <= 0
    qbar = 10;
else
    qbar =  options_specific.qbar;
end

% initialization
[n, p] = size(A);

if ~options_general.mode
    %%% un-pack Gram matrix if available; otherwise, compute Gram matrix. 
   if ~isempty(options_general.gram)
            AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
       else
        AtA = A'*A;
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A'*b;
end

global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
    f = @(x) norm(A * x - b)^2;
end

%%% 
if options_general.mv == 0
   if 1.1 * 2 * n < p
      mv = 2; 
   else
      mv = 1; 
   end
else
    mv = options_general.mv;   
end

maxiter = options_specific.itmax;
F = [];
Lambda = 1:p;
lambda = -Atb;
x = zeros(p, 1);
q = qbar;
ninf = p + 1;
iter = 0;
flag = 0; 

% gradient = lambda
perf = getL(options_general.perfmeas, lambda, x);
check = check_termination(t0, options_general, perf, lambda, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

% check 
if check.stop
   out.xopt = x; 
   out.err = f(x);
   out.ConvSpeed = ConvSpeed; 
   out.check = check;
end

while(~check.stop) && iter < maxiter
    fH1 = find(x(F) < -eps);
    H1 = F(x(F) < -eps);
    fH2 = find(lambda(Lambda) < -eps);
    H2 = Lambda(lambda(Lambda) < -eps);
    H1H2 = union(H1, H2);
    cardH1H2 = length(H1H2);
    if cardH1H2 < ninf
        ninf = cardH1H2;
        q = qbar;
    else
        if q > 0
           q = q - 1;
        else
           if length(F) >= min(n,p)
            r = H1(length(H1));  
           else
            r = H1H2(cardH1H2);
           end
           if ismember(r, H1)
              H1  = r;
              H2  = [];
              fH1 = find(F == r);
              fH2 = [];
           else
              H1  = []; 
              H2  = r;
              fH1 = [];
              fH2 = find(Lambda == r);
           end
        end
    end
        Fprime = F;
        Fprime(fH1) = [];
        F = [Fprime H2];
        %%% only relevant for a p > n scenario.
        if  length(F) > min(n,p)
            %%% remove indices such that F is small enough 
            lfH2 = min(n,p) - length(Fprime);
            [so, ix] = sort(lambda(H2));
            H2 = H2(ix(1:lfH2));
            F = [Fprime H2];
            Lambda = union(setdiff(Lambda, H2), H1);
        else
            Lambda = union(setdiff(Lambda, H2), H1);
            lfH2 = length(fH2);
        end
        %%%
        
        lfH1 = length(fH1);
        
        %%% 0.1: rule of thumb.
        if  ~flag && (lfH1 + lfH2) <= max(1, 0.1 * length(F))
            % use up- and downdating
            % downdating, maintaining ordering
            if lfH1 > 0 
                for j=1:lfH1
                    R = choldown(R, fH1(lfH1 - j + 1));
                end
            end
            % then updating
            if lfH2 > 0
                for j=1:lfH2
                    R = cholinsertgram(R, AtA(H2(j), H2(j)), AtA(H2(j), Fprime));
                    Fprime = [Fprime H2(j)];
                end
             end
          else
            R = chol(AtA(F,F));
            flag = 1;
        end
        x = zeros(p, 1);
        x(F) =  R \ (R' \ Atb(F));
        %%% mv
        if mv == 1
            lambda(Lambda) = AtA(Lambda, F) * x(F) - Atb(Lambda);
        else
            lambda(Lambda) = transpose(A(:,Lambda)) * (A(:, F) * x(F) - b);
        end
        %%%
        lambda(F) = 0.9 * eps;
        %%%
        iter = iter + 1;
        %%% gradient = lambda
        perf = getL(options_general.perfmeas, lambda, x);
        check = check_termination(t0, options_general, perf, lambda, x, check.fprev, check.xprev); 
        perf = perf(~isnan(perf));
        ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
        
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;    

end


% Fast Cholesky insert and remove functions
function R = cholinsertgram(R, diag_k, col_k)
if isempty(R)
  R = sqrt(diag_k);
else
%  col_k = x'*X; % 
  R_k = R'\col_k'; % R'R_k = (X'X)_k, solve for R_k
  R_kk = sqrt(diag_k - R_k'*R_k); % norm(x'x) = norm(R'*R), find last element by exclusion
  R = [R R_k; [zeros(1,size(R,2)) R_kk]]; % update R
end
end


