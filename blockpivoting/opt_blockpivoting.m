% Function to set-up options specific to
% block principal pivoting
%
% Inputs
%
% 'down' --- Option controlling downdating of the 
%            Cholesky factorization of the Gram matrix
%            after dropping variables from the current
%            active set.                                           
%               '1': Cholesky downdating based on MATLAB code.
%               '2': Cholesky downdating based on speed-optimized
%                    C-code.
% 
% 'itmax' --- upper bound on the number of iterations (integer).
%             Default 'inf', i.e. there is no such upper bound.
%        
%
% 'qbar'  --- Maximum number of successive failures in reducing
%             the number of infeasibilities until Murty's method
%             is used to reduce the number of infeasibilities  
%             (see 'A comparison of Block Pivoting and interior-point
%              algorithms for linear least squares problems
%              with nonnegative variables', 
%              Mathematics of Computation 63 (208), 625-643) 

function options = opt_blockpivoting(varargin)
  
p = inputParser;

validdown = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('down',1,validdown);

validitmax = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('itmax', inf, validitmax);

validqbar = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('qbar', 10, validqbar);

p.parse(varargin{:});
options = p.Results;

 
