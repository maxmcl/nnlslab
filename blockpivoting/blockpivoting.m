% Name of the NNLS solver:
% Block principal pivoting 
% 
% It is required that the Gram matrix A' * A can be computed an kept in storage.
% In general, the method fails to converge if the matrix A does not have
% full column rank. 
%
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_blockpivoting'.

function [out] = blockpivoting(A, b, options_general, options_specific)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if options_general.usegram      
        switch options_specific.down
            case 1
                   [out] = blockpivoting_down(A, b, options_general, options_specific);
            case 2
                   [out] = blockpivoting_downc(A, b, options_general, options_specific);              
        end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
else
    error('Block pivoting requires storage of the full Gram matrix')
end    