% Name of the NNLS solver:
% Accelerated projected gradient (projected gradient with extrapolation step) 
%
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_apg'.
%
% 
% Output
%
% A structure with the following fields
%
% xopt --- the iterate of the last solution (column vector), approximation to the optimal NNLS solution x^*, 
%            
% err  --- objective evaluated at xopt,
%
% ConvSpeed --- A matrix of dimension #iterations times #performance measures as specified in 'options_general' plus one.  
%               The first column of ConvSpeed always stores CPU times. The remaining columns store the corresponding
%               values of the performance measures.
%
% check --- A structure containing information about the termination status.
%           'check' contains the following fields.
%
%           check.stop:   Binary.
%                        '0': 	Solver stopped because of an internal stopping criterion,
%				e.g. because of numerical issues.
%                         1':   Solver stopped because a regular stopping criterion was met.
%                        
%                                 	    		            
%          check.reason: A vector of length to 0 to 4 listing the regular stopping criteria met upon termination.  
%                        
%			'0': Maximum cpu time was exceeded.   
%                       '1': Desired KKT optimality was achieved.
%                       '2': Difference of the objective values dropped below the specified stopping criterion.
%                       '3': Euclidean distance of the iterates to a specified ground truth x^* dropped
%                            below the specified stopping criterion. 
%     
%          check.tol:   Numerical tolerances associated with the reasons 1-3. 
%
%          check.xprev: Iterate of the penultimate iteration before termination.
%
%          check.fprev: Objective evaluated at check.xprev.
%
%  


function [out] = apg(A, b, options_general, options_specific)


if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

 if options_general.mode
     AtA = A;
     Atb = b;
     mv = 1;
 end
 
 %%% Initialize CPU time
 t0 = tic;
 %%%
 [n, p] = size(A);
 
 %%%
 if ~options_general.mode
     if options_general.mv == 0
         if 1.1 * 2 * n < p
             mv = 2;
         else
             mv = 1;
         end
     else
         mv = options_general.mv;
     end
 end
 
 if ~options_general.mode
    %%%
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
       if  mv == 1
           AtA = A'*A;
       end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% Initialize gradient

if mv == 1
   grad =@(x) (AtA * x - Atb); 
else
   grad =@(x) A' * (A * x - b); 
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) x' * (AtA * x) - 2 * x' * Atb; 
     else
        f = @(x) norm(A * x - b)^2;
     end
end

%%%
stepsel = options_specific.stepsel;

if stepsel == 1
    if  options_specific.L > 0;
        t = 1/options_specific.L * 0.99;
    else
        if mv == 1
            t = 1 / normest(AtA) * 0.99;
        else
            t = 1/(normest(A)^2) * 0.99;
        end
    end
end

%%% 

if stepsel == 3
    if mv == 1
       trace = sum(diag(AtA));
       t = 1 / trace;
    else
       trace = sum(sum(A.^2));
       t = 1 / trace;
    end
end

gradf = -Atb;
x = zeros(p, 1);
y = x;
if  stepsel == 2
    t = 1;
    beta = 0.9; % parameter for back-tracking.
end
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
%%%
%%%
k = 0;
%%%
FLAGOUTER = 0;

while ~check.stop && ~FLAGOUTER

    if ismember(stepsel, [1 3])
        xnew = max(0, y - t * gradf);
    else
        FLAG = 1;
        fy = 0.5 * f(y); 
        m = 0;
        while FLAG && t > eps
            t = t * beta^m;
            xnew = max(0, y -  t * gradf);
            fxnew = 0.5 * f(xnew); 
            if fxnew < fy + gradf' * (xnew - y) + 1/(2*t) * norm(xnew - y)^2;
                FLAG = 0;
            else
                m = m + 1;
            end
        end
        if t < eps
           FLAGOUTER = 1;
        end
    end
  
    y = xnew + k/(k+3) * (xnew - x);
    x = xnew;
    gradf = grad(y);
    k = k + 1;
    
    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed = [ConvSpeed; [toc(t0) perf]];
    
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end


