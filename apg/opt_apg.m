% Function to set-up options specific to
% accelerated projected gradient 
%
% Inputs
%
% 'stepsel' --- step-size selection scheme to be used.
%               '1': constant step-size based on the 
%                    Lipschitz constant of the gradient.
%               '2': Step-size selection via
%                    back-tracking (cf. e.g. Beck and Tebouille, 
%                    'A Fast Iterative Shrinkage-Thresholding Algorithm
%                     for Linear Inverse Problems', (2009), p.191.
%               '3': Fixed step-size using the trace of the Gram matrix 
%                    A' * A as a crude, but computationally inexpensive way to bound 
%                    the Lipschitz constant of the gradient.  
% 
% 'mvobj'  --- scheme to be used for comparison of objective values
%              '1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              '2': f(x) = norm(A * x).^2 (default).
%        
%
% 'L'      --- an upper bound on the Lipschitz constant of the gradient.
%              Default is '0', in which case 'normest' is called by
%              'apg' to upper bound the Lipschitz constant of the gradient. 

function options = opt_apg(varargin)

p = inputParser;

validstepsel = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',3});
p.addParamValue('stepsel', 1, validstepsel);

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('mvobj', 2, validmvobj);

validL = @(x)validateattributes(x,{'numeric'},{'scalar','positive'});
p.addParamValue('L', 0, validL);

p.parse(varargin{:});
options = p.Results;

% options.stepsel = stepsel;
% options.mvobj = mvobj;
% options.L = L;



