% Implementation of accelerated projected gradient (projected gradient with extrapolation step)
% tailored to the case of A being a partial Fourier matrix, cf. Donoho and Tanner 'Counting the faces of randomly-projected
% hypercubes and orthants, with applications' (2010), Section 4.2 
%
%
% Input
%
% p --- dimension of the solution vector (x). Must be an integer power of 2. 
%
% b --- observation vector
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_apg'.

function [out] = apg_f(p, b, options_general, options_specific)


%%% Initialize CPU time
t0 = tic;
%%%
n = length(b);

%%%
odds =  1:2:n;
evens = 2:2:(n-1);
m = length(odds);
%%%
imagix =  2:m;
%%%
% pre-compute constant part of the gradient
bcmp = complex(b(odds), [0; -b(evens)]);
bcmp = [bcmp; zeros(p - m, 1)];
Atb = real(ifft(bcmp) * p);
%
zeroes = zeros(p - m, 1);
ix = 1:m;
% construct the permutation for the synth function
ixx = zeros(n, 1);
ixx(odds) = 1:m;
ixx(evens) = (m+1):n;
%
truncate = @(x) x(ix);
getlow = @(x) truncate(fft(x));
put_real_imag = @(x) x(ixx);
synth = @(x) put_real_imag( vertcat( real(x), -imag(x(imagix)) ) );

grad = @(x)  real(ifft( vertcat( getlow(x), zeroes) ) * p) - Atb;


%%% Initialize objective
global f;
f = @(x) norm(b - synth(getlow(x)))^2;

%%%
 stepsel = options_specific.stepsel;
% 

gradf = -Atb;
x = zeros(p, 1);
y = x;
if  stepsel > 1
    t = 1;
    beta = 0.9; % parameter for back-tracking.
end
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
%
%%%
k = 0;
%%%
FLAGOUTER = 0;

while ~check.stop && ~FLAGOUTER

    if stepsel == 1
        xnew = max(0, y - t * gradf);
    else
        FLAG = 1;
        fy = 0.5 * f(y); 
        m = 0;
        while FLAG && t > eps
            t = t * beta^m;
            xnew = max(0, y -  t * gradf);
            fxnew = 0.5 * f(xnew); 
            if fxnew < fy + gradf' * (xnew - y) + 1/(2*t) * norm(xnew - y)^2;
                FLAG = 0;
            else
                m = m + 1;
            end
        end
        if t < eps
           FLAGOUTER = 1;
        end
    end
  
    y = xnew + k/(k+3) * (xnew - x);
    x = xnew;
    gradf = grad(y);
    k = k + 1;
    
    perf = getL(options_general.perfmeas, gradf, x);
    check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed = [ConvSpeed; [toc(t0) perf]];
    
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end


