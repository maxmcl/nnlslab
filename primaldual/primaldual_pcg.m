% Name of the NNLS solver:
%
% Primal-Dual interior point method.
% PCG (preconditioned conjugate gradient with a diagonal
% preconditioner) is used to (approximately) solve the linear systems
% associated with each Newton system. The use of PCG 
% in this code is an adaption of code available from  
% http://www.stanford.edu/~boyd/l1_ls/. 
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_primaldual'.


function [out] = primaldual_pcg(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
    error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
    error('Structure of stopcrit not compatible with structure of tol')
end

if options_general.mode
    AtA = A;
    Atb = b;
    %      sherman = 0;
    mv = 1;
end

 %%% Initialize CPU time
 t0 = tic;
 %%%
 [n, p] = size(A);
 
 %%%
 if ~options_general.mode
     if options_general.mv == 0
         if 1.1 * 2 * n < p
             mv = 2;
         else
             mv = 1;
         end
     else
         mv = options_general.mv;
     end
 end
 
%  sherman = options_specific.sherman;
%  
%  if sherman < 0
%    if (n^2 * p + n^3) < 0.25 * p^3 
%        sherman = 1;  
%    else
%        sherman = 0;
%    end
% end
 
if ~options_general.mode
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
        AtAdiag = diag(AtA);
        %usegram = 1;
    else
        if options_general.usegram
            AtA = A'*A;
            AtAdiag = diag(AtA);
            %usegram = 1;
        else
            AtAdiag = sum(A.^2)';
            %usegram = 0;
            mv = 2;
        end
    end
else
    AtAdiag = diag(AtA);
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end
 
% avoid double storage of Gram matrix
% if ~sherman
%     if  issparse(AtA) && p^2 > intmax
%         sparseindexing = 1;
% %         [ii, jj, val0] = find(AtA);
% %         iieqjj = (ii == jj);
% %         val0diag = val0(iieqjj);
%     else
%         sparseindexing = 0;
%         diagind = sub2ind([p,p], 1:p, 1:p)';
%         AtAdiag = diag(AtA);
%     end
    
    if mv == 1
        grad = @(x) (AtA * x - Atb);
        AXfunc = @(x, d, pp) AtA * x + d .* x; 
    else
        grad = @(x) A' * (A * x - b);
        AXfunc = @(x, d, pp) A' * (A * x) + d .* x;
    end
% else
%     Attilde = A';
%     diagind = sub2ind([n,n], 1:n, 1:n)';
%     grad = @(x) A' * (A * x - b);
% end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
         f = @(x) x' * (AtA * x) - 2 * x' * Atb;
     else
         f = @(x) norm(A * x - b)^2;
     end
end

%%%
Mfunc = @(x, d, pp) x .* pp;
%%% 

%%% functions to evaluate the (inverse) 'surrogate' duality gap 
%%% and the residual of the KKT system. 
gamma = @(x, lambda) p/(x' * lambda);
residual = @(x, lambda) sqrt(norm(grad(x) - lambda)^2 + norm(x .* lambda - 1/gamma(x, lambda))^2);

% Initialization
alpha = 0.01; beta=0.95; % parameters for the stepsize selection
FLAG = 1; 
x = ones(p,1);
descentX = zeros(p, 1);
lambda = 1./x;
r = residual(x, lambda);
mu = options_specific.mu;
%%%
gradf = grad(x);    
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;


 while FLAG && (~check.stop)
     
     GammaVal = mu * gamma(x, lambda); 
     
    
     RHS = -gradf + 1/GammaVal./x;
     D = lambda./x;
    
     % compute the (truncated) Newton descent direction using pcg,
     % following Koh, Kim and Boyd.
     
     prb = AtAdiag + D;

     normg   = norm(RHS);
     gap = getkktopt(gradf, x);
     pcgtol  = min(1e-1, options_specific.pcgtol * gap/min(1, normg));
    
     if 1
         [descentX, pflg, prelres, pitr, presvec] = ...
             pcg(AXfunc, RHS, pcgtol, options_specific.pcgmaxiter, Mfunc,...
             [],descentX, D, 1./prb);
     end
     
     
%      if (pflg == 1) pitr = options_specific.pcgmaxiter; end
     
     %
     
     descentL = -lambda + 1/GammaVal./x - (lambda./x).*descentX;
     
     % get the stepsize with the Armijo rule
     xold = x;
     lambdaold = lambda;
     rold = r;
     FLAGSTEP = 1;
     
     ix1 = find(descentL < 0);
     if ~isempty(ix1)
         min1 = min(1, min(-lambda(ix1)./descentL(ix1)));
     else
         min1 = 1;
     end
     
     ix2 = find(descentX < 0);
     
     if ~isempty(ix2)
         min2 = min(1, min(-x(ix2)./descentX(ix2)));
     else
         min2 = 1;
     end
      
     t = 0.99*min(min1,min2);
    
     while FLAGSTEP
        x = xold + t * descentX;
        lambda = lambdaold + t*descentL;
        %%% compute new residuals
        r = residual(x, lambda);
        if( r > (1-alpha*t)*rold)
        t = beta * t;
        else
            FLAGSTEP = 0;
        end  
     end
     %%% udpate gradient
     gradf = grad(x);
     %%%
     perf = getL(options_general.perfmeas, gradf, x);
     check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
     perf = perf(~isnan(perf));
     ConvSpeed = [ConvSpeed; [toc(t0) perf]];
     

     %disp(['surrogate duality gap: ' num2str(1/gamma(x, lambda)) '  residual: ' num2str(r)])
     %%% termination criterion
     if(1/gamma(x, lambda) < eps && r < eps) 
         FLAG =0;
     end
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end










  

