% Function to set-up options specific to
% the primal-dual interior method.
%
% Inputs
%
% 'sherman' --- A value from {-1,0,1} that is used
%               to determine whether the Sherman-Woodbury-
%               matrix identity should be invoked to
%               solve the linear systems associated with
%               the Newton steps. This is useful if      
%               A is a fat matrix. If 'sherman = 1' ('sherman' = 0)
%               then this option is (not) used. If 'sherman = -1',
%               (default), the choice is made automatically made
%               in 'logbarrier.m' based on the dimensions of A.  
%
% 'mvobj'   ---  scheme to be used for comparison of objective values
%              	'1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              	'2': f(x) = norm(A * x).^2 (default).          
%    
%  'mu' 	--- tuning parameter (>1) as in Boyd and Vandenberghe (2004), Ch.11.7, p.612.
%                   (default: 5) 
%             


function options = opt_primaldual(varargin)
p = inputParser;

validsherman = @(x)validateattributes(x,{'numeric'},{'scalar','integer','>=',-1,'<=',1});
p.addParamValue('sherman',-1, validsherman);

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('mvobj', 2, validmvobj);

validmu = @(x)validateattributes(x,{'numeric'},{'scalar','positive','>',1});
p.addParamValue('mu', 5, validmu);

p.parse(varargin{:});
options = p.Results;



