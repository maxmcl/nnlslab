% Function to set-up options specific to
% the Lawson-Hanson algorithm
%
% Inputs
%
% 'up'  --- Updating of the Cholesky factorization 
%           is done ('up' = 1) or not done ('up' = 0).
%           In the second case, a Cholesky factorization
%           is re-computed from scratch each time
%           a new variable is added into the active set.  
%
% 'down' --- Downdating of the Cholesky factorization
%            is done using MATLAB code ('down' = 1),
%            C-code ('down' = 2) or not done ('down' = 0).
%
% 'outeritmax' --- maximum number of outer iterations (integer).
%                  default: 'inf'
%                 
% 'inneritmax' --- maximum number of inner iterations (integer).
%                  default: 'inf'
             

function options = opt_lawsonhanson(varargin)

p = inputParser;

validup = @(x)validateattributes(x,{'numeric'},{'scalar','binary'});
p.addParamValue('up',1,validup);

validdown = @(x)validateattributes(x,{'numeric'},{'scalar','integer','nonnegative','<=',2});
p.addParamValue('down',1,validdown);

validouteritmax = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('outeritmax', inf, validouteritmax);

validinneritmax = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('inneritmax', inf, validinneritmax);

p.parse(varargin{:});
options = p.Results;

if options.down~=0 && options.up == 0
    options.up = 1;
end


