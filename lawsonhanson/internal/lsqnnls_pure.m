% *internal code not to be called directly by the user*
function [out] = lsqnnls_pure(A, b, options_general, options_specific)
% solve NNLS problem by Lawson-Hanson's algorithm
% Cholesky updating/downdating used

%%% Initialize objective -- global [possibly unused]
global f;
f = @(x) norm(A * x - b)^2;
%%% Initialize CPU time
t0 = tic;
%initialize variables
[n, d] = size(A);
x = zeros(d,1);

% active/passive set
P = false(d,1);
Z = true(d,1);
wz = zeros(d,1);
w = A' * b;

outeriter = 1; outeritmax = options_specific.outeritmax; 
iter = 0; itmax = options_specific.inneritmax; 

% grad = -w_
perf = getL(options_general.perfmeas, -w, x);
check = check_termination(t0, options_general, perf, -w, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

% check 
if check.stop
   out.xopt = x; 
   out.err = f(x);
   out.ConvSpeed = ConvSpeed; 
   out.check = check;
end


% outer loop to put variables into set to hold positive coefficients
while any(Z) && any(w(Z) > eps) && (~check.stop)
    
    if outeriter == outeritmax
       disp('Exiting: Outer iteration count is exceeded.');
       break;
    end
    
    outeriter = outeriter + 1;
    % Move k from active set to positive set
    z = zeros(d,1); 
    wz(P) = -Inf;
    wz(Z) = w(Z);
    % Find variable with largest Lagrange mutliplier
    [unused,t] = max(wz);
    % Move variable t from zero set to positive set
    P(t) = true;
    Z(t) = false;
    % Compute intermediate solution using only variables in positive set
    z(P) = A(:,P) \ b;
    
    % check feasibility of z, if not reset x and compute z again
    while any(z(P) <= eps)
        iter = iter + 1;
        if iter > itmax
           disp('Exiting: Inner iteration count is exceeded.');
           out.xopt = x; 
           out.err = f(x);
           out.ConvSpeed = ConvSpeed; 
           out.check = check;
           return
        end
        
       % Find indices where temporary solution xnew is approximately
       % negative, backtrack x and reset Z/P(move some indice out from P),
       % and recompute x by Cholesky downdating
       Q = (z <= eps) & P;
       alpha = min(x(Q)./(x(Q) - z(Q)));
       if ~isfinite(alpha)
           disp('Warning: Inner iteration left. No strictly positive stepsize found.');
           out.xopt = x; 
           out.err = f(x);
           out.ConvSpeed = ConvSpeed; 
           out.check = check;
           return
       end
       x = x + alpha*(z - x);
       % Reset Z and P given intermediate values of x
       Z = ((abs(x) < eps) & P) | Z; % remove indice st < tol from P to Z
       P = ~Z;
       z = zeros(d,1);           % Reset z
       z(P) = A(:,P)\ b;      % Re-solve for z   
    end
    x = z;
    w = A' * (b - A(:,P) * x(P));
       
    perf = getL(options_general.perfmeas, -w, x);
    check = check_termination(t0, options_general, perf, -w, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end