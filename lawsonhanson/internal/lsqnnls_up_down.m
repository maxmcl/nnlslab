% *internal code not to be called directly by the user*
function [out] = lsqnnls_up_down(A, b, options_general, options_specific)
% solve NNLS problem by Lawson-Hanson's algorithm
% Cholesky updating/downdating used

%%% Initialize objective -- global [possibly unused]
global f;
f = @(x) norm(A * x - b)^2;
%%% Initialize CPU time
t0 = tic;
%initialize variables
[n, d] = size(A);
Atb = A' * b;

% active/passive set
P = []; 
Z = 1:d;  % switch from Boolean representation [Q.] to index representation [M.]
Pnew = P; 
sizeP = 0; 
sizePnew = 0; % number of points already added into P
%

Lt = []; % upper triangular matrix for Cholesky Decomposition
x = zeros(d, 1); % current iterate
xnew = zeros(d, 1); % iterate obtained after solving the LS problem.
y = []; % temporary vector of variable size
w = A' * (b - A*x);

outeriter = 1; outeritmax = options_specific.outeritmax; 
iter = 0; itmax = options_specific.inneritmax; 

% grad = -w_
perf = getL(options_general.perfmeas, -w, x);
check = check_termination(t0, options_general, perf, -w, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;
% check 
if check.stop
   out.xopt = x; 
   out.err = f(x);
   out.ConvSpeed = ConvSpeed; 
   out.check = check;
end


% outer loop to put variables into set to hold positive coefficients
while ~isempty(Z) && any(w(Z) > eps) && (~check.stop)
    
    if outeriter == outeritmax
       disp('Exiting: Outer iteration count is exceeded.');
       break;
    end
    
    outeriter = outeriter + 1;
    % Move k from active set to positive set
    wz = w(Z);
    [unused, k] = max(wz);
    k = Z(k);
    Pnew = [P k]; 
    Z = setdiff(Z, k); 
    sizePnew = length(Pnew); 
    
    % Cholesky Updating to get xnew (temporary)
    xnew = zeros(d,1);
    
    if sizePnew == 1
        Lt(1,1) = norm(A(:,k));
        y(1) = Lt(1,1) \ Atb(k);
        xnew(k) = Lt(1,1) \ y(1);
    else
        l = Lt' \ (transpose(A(:,P)) * A(:, k));
        lkk = sqrt(sum(A(:,k).^2) - sum(l.^2));
        Lt(1:sizeP,  sizePnew) = l; 
        Lt(sizePnew, sizePnew) = lkk;
        y = [y; (Atb(k) - dot(l,y)) / lkk];
        xnew(Pnew) = Lt \ y;
    end       
    P = Pnew;
    sizeP = sizePnew;
    
    % check feasibility of xnew, if not reset x and compute xnew again
    while any( xnew(P) <= eps )
        iter = iter + 1;
        if iter > itmax
           disp('Exiting: Inner iteration count is exceeded.');
           out.xopt = x; 
           out.err = f(x);
           out.ConvSpeed = ConvSpeed; 
           out.check = check;
           return
        end
        
       % Find indices where temporary solution xnew is approximately
       % negative, backtrack x and reset Z/P(move some indice out from P),
       % and recompute x by Cholesky downdating
       Q = P(xnew(P) <= eps);
       alpha = min(x(Q)./(x(Q) - xnew(Q)));
       if ~isfinite(alpha)
           disp('Warning: Inner iteration left. No strictly positive stepsize found.');
           out.xopt = x; 
           out.err = f(x);
           out.ConvSpeed = ConvSpeed; 
           out.check = check;
           return
       end
       x = x + alpha * (xnew - x);

       
       tmp = find(abs(x(P)) < eps);
       Z = [Z P(tmp)]; 
       P(tmp) = [];
       
       
       % Cholesky Downdating
       for i = length(tmp):-1:1   
           Lt = choldownmatlab(Lt, tmp(i));               
           sizeP = sizeP - 1;
       end 
       
       % solve for x/y  
       y = Lt' \ Atb(P);
       xnew(P) = Lt \ y; 
       
    end
    x = xnew;
    w = A' * (b - A(:,P) * x(P));
    
    perf = getL(options_general.perfmeas, -w, x);
    check = check_termination(t0, options_general, perf, -w, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end

function Lkt =  choldownmatlab(Lt, k) 
% This is for non-sparse matrix
% cholesky downdating
% A in R^(n,p)
% G = A'* A = L * L', where L, L' come from cholesky decomposition
% now  removes kth column from A, denoted by Ak. Gk := Ak' * Ak
% Given L' and k, choldown computes the chol. decomposition of  Gk
% i.e. Lk' * Lk = Gk, without processing of A, G

p = length(Lt);

% drop the kth clm of Lt
Temp = Lt;
Temp(:,k) = []; % Temp in R^(p,p-1)

% Givens Rotations
for i = k:p-1,
    a = Temp(i,i);
    b = Temp(i+1,i);
    r = sqrt(sum(Lt(:,i+1).^2) - sum(Temp(1:i-1,i).^2));
    c =  r * a / (a^2+b^2);
    s =  r * b / (a^2+b^2);
    % ith row of rotation matrix H
    Hrowi = zeros(1,p); Hrowi(i) = c; Hrowi(i+1) = s; 
    % (i+1)th row of ration matrix H
    Hrowi1 = zeros(1,p); Hrowi1(i) = -s; Hrowi1(i+1) = c;
    % modify the ith and (i+1)th rows of Temp
    v = zeros(2,p-1);
    v(1,i:p-1) = Hrowi * Temp(:,i:p-1);
    v(2,i+1:p-1) = Hrowi1 * Temp(:,i+1:p-1);
    Temp(i:i+1,:) =  v;
end

% drop the last row
Lkt = Temp(1:p-1,:);
end