% *internal code not to be called directly by the user*
function [out] = fnnls_pure(A, b, options_general, options_specific)
% solve NNLS problem by Lawson-Hanson's algorithm
% Gram matrix stored

if ~options_general.mode
   AtA = A;
   Atb = b;
end

%%% Initialize CPU time
t0 = tic;
%%%
[n, d] = size(A);

%%% 
if options_general.mv == 0
   if 1.1 * 2 * n < d
      mv = 2; 
   else
      mv = 1; 
   end
else
    mv = options_general.mv;   
end

%%% un-pack Gram matrix if available; otherwise, compute Gram matrix.
if options_general.mode
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [d d])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
        AtA = A'*A;
    end
end
%%%

%initialize variables
if options_general.mode 
    Atb = A' * b;
end

%%% Initialize objective -- global [possibly unused]
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
    f = @(x) norm(A * x - b)^2;
end

%%%
P = false(d,1); Z = true(d,1); % Initialize passive set P (xi>0) to null, Z (xi=0) to all
x = zeros(d,1); % final solution, unordered solution x and new temporary solution after an iteration 

w = Atb;
wz = zeros(d,1);% -f'(x) 


outeriter = 1; outeritmax = options_specific.outeritmax; 
iter = 0; itmax = options_specific.inneritmax; 

% grad = -w_
perf = getL(options_general.perfmeas, -w, x);
check = check_termination(t0, options_general, perf, -w, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;
% check 
if check.stop
   out.xopt = x; 
   out.err = f(x);
   out.ConvSpeed = ConvSpeed; 
   out.check = check;
end

% outer loop to put variables into set to hold positive coefficients
while any(Z) && any(w(Z) > eps) && (~check.stop)  
    if outeriter == outeritmax
       disp('Exiting: Outer iteration count is exceeded.');
       break;
    end
   
   outeriter = outeriter + 1;
   % Reset intermediate solution z
   z = zeros(d,1); 
   wz(P) = -Inf;
   wz(Z) = w(Z);
   % Find variable with largest Lagrange mutliplier
   [unused, t] = max(wz);
   % Move variable t from zero set to positive set
   P(t) = true;
   Z(t) = false;
   % Compute intermediate solution using only variables in positive set
   z(P) = A(:,P) \ b;
   % inner loop to remove elements from the positive set which no longer belong
   while any(z(P) <= eps)
       iter = iter + 1;
       if iter > itmax
           disp('Exiting: Inner iteration count is exceeded.');
           out.xopt = x; 
           out.err = f(x);
           out.ConvSpeed = ConvSpeed; 
           out.check = check;
           return
       end
       
       % Find indices where intermediate solution z is approximately negative
       Q = (z <= eps) & P;
       % Choose new x subject to keeping new x nonnegative
       alpha = min(x(Q)./(x(Q) - z(Q)));
       if ~isfinite(alpha)
           disp('Warning: Inner iteration left. No strictly positive stepsize found.');
           out.xopt = x; 
           out.err = f(x);
           out.ConvSpeed = ConvSpeed; 
           out.check = check;
           return
       end
       x = x + alpha*(z - x);
       % Reset Z and P given intermediate values of x
       Z = ((abs(x) < eps) & P) | Z; % remove indice st < tol from P to Z
       P = ~Z;
       z = zeros(d,1);           % Reset z
       z(P) = A(:,P)\ b;      % Re-solve for z
   end
   x = z;
   if mv == 1
      w = Atb - AtA(:,P) * x(P);
   else
      w = A' * (b - A(:,P) * x(P)); 
   end
   
   perf = getL(options_general.perfmeas, -w, x);
   check = check_termination(t0, options_general, perf, -w, x, check.fprev, check.xprev);
   perf = perf(~isnan(perf));
   ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
   
end

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end