% Name of the NNLS solver:
% Lawson-Hanson algorithm.
%
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_lawsonhanson'.

function [out] = lawsonhanson(A, b, options_general, options_specific)

tic;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
    error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
    error('Structure of stopcrit not compatible with structure of tol')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if options_general.usegram
    
    if options_specific.up
        
        switch options_specific.down
            case 0
                [out] = fnnls_up(A, b, options_general, options_specific);
            case 1
                [out] = fnnls_up_down(A, b, options_general, options_specific);
            case 2
                [out] = fnnls_up_down_c(A, b, options_general, options_specific);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else
        [out] = fnnls_pure(A, b, options_general, options_specific);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    
    if options_specific.up
        
        switch options_specific.down
            case 0
                [out] = lsqnnls_up(A, b, options_general, options_specific);
            case 1
                [out] = lsqnnls_up_down(A, b, options_general, options_specific);
            case 2
                [out] = lsqnnls_up_down_c(A, b, options_general, options_specific);
        end
    else
        [out] =  lsqnnls_pure(A,b, options_general, options_specific);
    end
end
out.time = toc;
end