% Function to set-up options specific to 
% spectral projected gradient (SPG).
%
% Inputs              
%
% 'mvobj'  --- scheme to be used for comparison of objective values
%              	'1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              	'2': f(x) = norm(A * x).^2 (default).
%
%
% 'M'      --- Back-tracking line search based on the non-monotonic Armijo
%              condition with the M previous objective values is performed.
%              default: 'M = 10'.   
%

function options = opt_spg(varargin)

p = inputParser;

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('mvobj', 2, validmvobj);

validM = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('M', 10, validM); 

p.parse(varargin{:});
options = p.Results;