% Name of the NNLS solver:
% Spectral projected gradient (SPG). 
% The code here is an adaptation of an implementation in  
% the MATLAB package 'minconf' by M. Schmidt, available from 
% http://www.di.ens.fr/~mschmidt/Software/minConf.html.
% 
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_spg'.
%
%
function [out] = spg(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

if options_general.mode
    AtA = A;
    Atb = b;
    mv = 1;
end

%%% Initialize CPU time
t0 = tic;
%%%
[n, p] = size(A);

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end

if ~options_general.mode
    %%% un-pack Gram matrix if available; otherwise, compute Gram matrix.
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
        if  mv == 1
            AtA = A'*A;
        end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% Initialize gradient

if mv == 1
   grad =@(x) (AtA * x - Atb); 
else
   grad =@(x) A' * (A * x - b); 
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) (x' * (AtA * x - 2 * Atb));  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) (x' * (AtA * x) - 2 * x' * Atb); 
     else
        f = @(x) norm(A * x - b)^2;
     end
end


%%% some stuff to be defined
funProj = @(x) x .* (x >= 0);
%%% constant for the back-tracking line search
suffDec = 1e-4; % default
%%%
g = -Atb;
x = zeros(p, 1);
fx = 0.5 * f(x);
%%%
perf = getL(options_general.perfmeas, g, x);
check = check_termination(t0, options_general, perf, g, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

%%%

M = options_specific.M;

i = 1;
while ~check.stop
    % Compute Step Direction
    if i == 1
        alpha = 1;
    else
        y = g - g_old;
        s = x - x_old;
        %         if bbType == 1
        alpha = (s'*s)/(s'*y);
        %         else
        %             alpha = (s'*y)/(y'*y);
        %         end
        if alpha <= 1e-10 || alpha > 1e10 || isnan(alpha)
           alpha = 1;
        end
    end
    d = -alpha*g; %alpha: BB step-size.
    %fx_old = fx;
    x_old = x;
    g_old = g;
    
    % Compute Projected Step
    d = funProj(x+d) - x;
    % Check that Progress can be made along the direction
    gtd = g'*d; 
    % Select Initial Guess to step length
    if i == 1
       t = min(1, 1/sum(abs(g)));
    else
        t = 1;
    end

    % Compute reference function for non-monotone condition
    
    if  M == 1
        fx_ref = fx;
    else
        if i == 1
            old_fvals = repmat(-inf,[M 1]); %%% generate an array with M entries
        end
        
        if i <= M
            old_fvals(i) = fx; 
        else
            old_fvals = [old_fvals(2:end);fx]; %%% remove the oldest function value
        end
        fx_ref = max(old_fvals); %%% select reference value for comparison
    end

    x_new = x + t*d;
    fx_new = 0.5 * f(x_new);

    % Backtracking Line Search
    lineSearchIters = 1;
    while fx_new > fx_ref + suffDec* g'*(x_new - x) 
        temp = t;
        
        if lineSearchIters < 2
            t = polyinterp([0 fx gtd; t fx_new sqrt(-1)]);
        else
            t = polyinterp([0 fx gtd; t fx_new sqrt(-1);t_prev f_prev sqrt(-1)]);
        end
        
        % Adjust if change is too small
        if t < temp*1e-3
            t = temp*1e-3;
        elseif t > temp*0.6
            t = temp*0.6;
        end


        % Evaluate New Point
        f_prev = fx_new;
        t_prev = temp;
        x_new = x + t*d;
        %       
        fx_new = 0.5 * f(x_new);
        lineSearchIters = lineSearchIters+1;
    end

    % Take Step
    x = x_new;
    fx = fx_new;
    g = grad(x);
    
    % check performance again
    perf = getL(options_general.perfmeas, g, x);
    check = check_termination(t0, options_general, perf, g, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    
    
    i = i + 1;
end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end

%%% Additional function for line search. 

function [minPos,fmin] = polyinterp(points,doPlot,xminBound,xmaxBound)

if nargin < 2
    doPlot = 0;
end

nPoints = size(points,1);
order = sum(sum((imag(points(:,2:3))==0)))-1;

% Code for most common case:
%   - cubic interpolation of 2 points
%       w/ function and derivative values for both
%   - no xminBound/xmaxBound

if nPoints == 2 && order ==3 && nargin <= 2 && doPlot == 0
    [minVal minPos] = min(points(:,1));
    notMinPos = -minPos+3;
    d1 = points(minPos,3) + points(notMinPos,3) - 3*(points(minPos,2)-points(notMinPos,2))/(points(minPos,1)-points(notMinPos,1));
    d2 = sqrt(d1^2 - points(minPos,3)*points(notMinPos,3));
    if isreal(d2)
        t = points(notMinPos,1) - (points(notMinPos,1) - points(minPos,1))*((points(notMinPos,3) + d2 - d1)/(points(notMinPos,3) - points(minPos,3) + 2*d2));
        minPos = min(max(t,points(minPos,1)),points(notMinPos,1));
    else
        minPos = mean(points(:,1));
    end
    return;
end

xmin = min(points(:,1));
xmax = max(points(:,1));

% Compute Bounds of Interpolation Area
if nargin < 3
    xminBound = xmin;
end
if nargin < 4
    xmaxBound = xmax;
end

% Constraints Based on available Function Values
A = zeros(0,order+1);
b = zeros(0,1);
for i = 1:nPoints
    if imag(points(i,2))==0
        constraint = zeros(1,order+1);
        for j = order:-1:0
            constraint(order-j+1) = points(i,1)^j;
        end
        A = [A;constraint];
        b = [b;points(i,2)];
    end
end

% Constraints based on available Derivatives
for i = 1:nPoints
    if isreal(points(i,3))
        constraint = zeros(1,order+1);
        for j = 1:order
            constraint(j) = (order-j+1)*points(i,1)^(order-j);
        end
        A = [A;constraint];
        b = [b;points(i,3)];
    end
end

% Find interpolating polynomial
params = A\b;

% Compute Critical Points
dParams = zeros(order,1);
for i = 1:length(params)-1
    dParams(i) = params(i)*(order-i+1);
end

if any(isinf(dParams))
    cp = [xminBound;xmaxBound;points(:,1)].';
else
    cp = [xminBound;xmaxBound;points(:,1);roots(dParams)].';
end

% Test Critical Points
fmin = inf;
minPos = (xminBound+xmaxBound)/2; % Default to Bisection if no critical points valid
for xCP = cp
    if imag(xCP)==0 && xCP >= xminBound && xCP <= xmaxBound
        fCP = polyval(params,xCP);
        if imag(fCP)==0 && fCP < fmin
            minPos = real(xCP);
            fmin = real(fCP);
        end
    end
end
% Plot Situation
if doPlot
    figure(1); clf; hold on;

    % Plot Points
    plot(points(:,1),points(:,2),'b*');

    % Plot Derivatives
    for i = 1:nPoints
        if isreal(points(i,3))
            m = points(i,3);
            b = points(i,2) - m*points(i,1);
            plot([points(i,1)-.05 points(i,1)+.05],...
                [(points(i,1)-.05)*m+b (points(i,1)+.05)*m+b],'c.-');
        end
    end

    % Plot Function
    x = min(xmin,xminBound)-.1:(max(xmax,xmaxBound)+.1-min(xmin,xminBound)-.1)/100:max(xmax,xmaxBound)+.1;
    size(x)
    for i = 1:length(x)
        f(i) = polyval(params,x(i));
    end
    plot(x,f,'y');
    axis([x(1)-.1 x(end)+.1 min(f)-.1 max(f)+.1]);

    % Plot Minimum
    plot(minPos,fmin,'g+');
    if doPlot == 1
        pause(1);
    end
end
end


