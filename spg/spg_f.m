function [out] = spg_f(p, b, options_general, options_specific)
% spectral projected gradient (SPG). Code adapted from M. Schmidt, 
% minConf_SPG package.

%%% Initialize CPU time
t0 = tic;
%%%
n = length(b);

%%%
odds =  1:2:n;
evens = 2:2:(n-1);
m = length(odds);
%%%
imagix =  2:m;
%%%
% pre-compute constant part of the gradient
bcmp = complex(b(odds), [0; -b(evens)]);
bcmp = [bcmp; zeros(p - m, 1)];
Atb = real(ifft(bcmp) * p);
%
zeroes = zeros(p - m, 1);
ix = 1:m;
% construct the permutation for the synth function
ixx = zeros(n, 1);
ixx(odds) = 1:m;
ixx(evens) = (m+1):n;
%
truncate = @(x) x(ix);
getlow = @(x) truncate(fft(x));
put_real_imag = @(x) x(ixx);
synth = @(x) put_real_imag( vertcat( real(x), -imag(x(imagix)) ) );

grad = @(x)  real(ifft( vertcat( getlow(x), zeroes) ) * p) - Atb;


%%% Initialize objective
global f;
f = @(x) norm(b - synth(getlow(x)))^2;

%%% some stuff to be defined
funProj = @(x) x .* (x >= 0);
%%% constant for the back-tracking line search
suffDec = 1e-4; % default
%%%
g = -Atb;
x = zeros(p, 1);
fx = 0.5 * f(x);
%%%
% Make objective function (if using numerical derivatives)
% funEvalMultiplier = 1;
% if numDiff
%     if numDiff == 2
%         useComplex = 1;
%     else
%         useComplex = 0;
%     end
%     funObj = @(x)autoGrad(x,useComplex,funObj);
%     funEvalMultiplier = nVars+1-useComplex;
% end

% % Evaluate Initial Point
% if ~feasibleInit
%     x = funProj(x);
% end
% [f,g] = funObj(x);

% % Optionally check optimality
% if testOpt
%     projects = projects+1;
%     if max(abs(funProj(x-g)-x)) < optTol
%         if verbose >= 1
%         fprintf('First-Order Optimality Conditions Below optTol at Initial Point\n');
%         end
%         return;
%     end
% end


%%%
perf = getL(options_general.perfmeas, g, x);
check = check_termination(t0, options_general, perf, g, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% fprev = inf; xprev = x + inf;

%%%

M = options_specific.M;

i = 1;
while ~check.stop
    % Compute Step Direction
    if i == 1
        alpha = 1;
    else
        y = g - g_old;
        s = x - x_old;
        %         if bbType == 1
        alpha = (s'*s)/(s'*y);
        %         else
        %             alpha = (s'*y)/(y'*y);
        %         end
        if alpha <= 1e-10 || alpha > 1e10
           alpha = 1;
        end
    end
    d = -alpha*g; %alpha: BB step-size.
    %fx_old = fx;
    x_old = x;
    g_old = g;
    
    % Compute Projected Step
    %     if ~curvilinear
    d = funProj(x+d) - x;
%     projects = projects+1;
%     end

    % Check that Progress can be made along the direction
     gtd = g'*d; % used below for the line search
%     if gtd > -progTol
%         %         if verbose >= 1
%         %             fprintf('Directional Derivative below progTol\n');
%         %         end
%         break;
%     end

    % Select Initial Guess to step length
    if i == 1
       t = min(1, 1/sum(abs(g)));
    else
        t = 1;
    end

    % Compute reference function for non-monotone condition
    
    if  M == 1
        fx_ref = fx;
    else
        if i == 1
            old_fvals = repmat(-inf,[M 1]); %%% generate an array with M entries
        end
        
        if i <= M
            old_fvals(i) = fx; %% CHECK
        else
            old_fvals = [old_fvals(2:end);fx]; %%% remove the oldest function value
        end
        fx_ref = max(old_fvals); %%% select reference value for comparison
    end

    % Evaluate the Objective and Gradient at the Initial Step
    %     if curvilinear
    %         x_new = funProj(x + t*d);
    %         projects = projects+1;
    %     else
    x_new = x + t*d;
    %     end
    fx_new = 0.5 * f(x_new);
%     funEvals = funEvals+1;

    % Backtracking Line Search
    lineSearchIters = 1;
    while fx_new > fx_ref + suffDec* g'*(x_new - x) %|| ~isLegal(f_new)
        temp = t;
        %         if interp == 0 || ~isLegal(f_new)
        %             if verbose == 3
        %                 fprintf('Halving Step Size\n');
        %             end
        % t = t/2;
        %         elseif interp == 2 && isLegal(g_new)
        %             if verbose == 3
        %                 fprintf('Cubic Backtracking\n');
        %             end
        %             t = polyinterp([0 f gtd; t f_new g_new'*d]);
        if lineSearchIters < 2
            %             if verbose == 3
            %                 fprintf('Quadratic Backtracking\n');
            %             end
            t = polyinterp([0 fx gtd; t fx_new sqrt(-1)]);
        else
            %             if verbose == 3
            %                 fprintf('Cubic Backtracking on Function Values\n');
            %             end
            t = polyinterp([0 fx gtd; t fx_new sqrt(-1);t_prev f_prev sqrt(-1)]);
        end
        
        % Adjust if change is too small
        if t < temp*1e-3
            %             if verbose == 3
            %                 fprintf('Interpolated value too small, Adjusting\n');
            %             end
            t = temp*1e-3;
        elseif t > temp*0.6
            %             if verbose == 3
            %                 fprintf('Interpolated value too large, Adjusting\n');
            %             end
            t = temp*0.6;
        end

        % Check whether step has become too small
%         if max(abs(t*d)) < progTol || t == 0
%             %             if verbose == 3
%             %                 fprintf('Line Search failed\n');
%             %             end
%             t = 0;
%             f_new = f;
%             g_new = g;
%             break;
%         end

        % Evaluate New Point
        f_prev = fx_new;
        t_prev = temp;
        %         if curvilinear
        %             x_new = funProj(x + t*d);
        %             projects = projects+1;
        %         else
        x_new = x + t*d;
        %         end
        fx_new = 0.5 * f(x_new);
        %funEvals = funEvals + 1;
        lineSearchIters = lineSearchIters+1;
    end

    % Take Step
    x = x_new;
    fx = fx_new;
    g = grad(x);
    
    % check performance again
    perf = getL(options_general.perfmeas, g, x);
    check = check_termination(t0, options_general, perf, g, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    
    

%     if testOpt
%         optCond = max(abs(funProj(x-g)-x));
%         projects = projects+1;
%     end

    % Output Log
%     if verbose >= 2
%         if testOpt
%             fprintf('%10d %10d %10d %15.5e %15.5e %15.5e\n',i,funEvals*funEvalMultiplier,projects,t,f,optCond);
%         else
%             fprintf('%10d %10d %10d %15.5e %15.5e\n',i,funEvals*funEvalMultiplier,projects,t,f);
%         end
%     end

    % Check optimality
%     if testOpt
%         if optCond < optTol
%             if verbose >= 1
%             fprintf('First-Order Optimality Conditions Below optTol\n');
%             end
%             break;
%         end
%     end
% 
%     if max(abs(t*d)) < progTol
%         if verbose >= 1
%             fprintf('Step size below progTol\n');
%         end
%         break;
%     end
% 
%     if abs(f-f_old) < progTol
%         if verbose >= 1
%             fprintf('Function value changing by less than progTol\n');
%         end
%         break;
%     end
% 
%     if funEvals*funEvalMultiplier > maxIter
%         if verbose >= 1
%             fprintf('Function Evaluations exceeds maxIter\n');
%         end
%         break;
%     end



    i = i + 1;
end

%%% QUIT & RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end

%%% Additional function for line search. 

function [minPos,fmin] = polyinterp(points,doPlot,xminBound,xmaxBound)
% function [minPos] = polyinterp(points,doPlot,xminBound,xmaxBound)
%
%   Minimum of interpolating polynomial based on function and derivative
%   values
%
%   In can also be used for extrapolation if {xmin,xmax} are outside
%   the domain of the points.
%
%   Input:
%       points(pointNum,[x f g])
%       doPlot: set to 1 to plot, default: 0
%       xmin: min value that brackets minimum (default: min of points)
%       xmax: max value that brackets maximum (default: max of points)
%
%   set f or g to sqrt(-1) if they are not known
%   the order of the polynomial is the number of known f and g values minus 1

if nargin < 2
    doPlot = 0;
end

nPoints = size(points,1);
order = sum(sum((imag(points(:,2:3))==0)))-1;

% Code for most common case:
%   - cubic interpolation of 2 points
%       w/ function and derivative values for both
%   - no xminBound/xmaxBound

if nPoints == 2 && order ==3 && nargin <= 2 && doPlot == 0
    % Solution in this case (where x2 is the farthest point):
    %    d1 = g1 + g2 - 3*(f1-f2)/(x1-x2);
    %    d2 = sqrt(d1^2 - g1*g2);
    %    minPos = x2 - (x2 - x1)*((g2 + d2 - d1)/(g2 - g1 + 2*d2));
    %    t_new = min(max(minPos,x1),x2);
    [minVal minPos] = min(points(:,1));
    notMinPos = -minPos+3;
    d1 = points(minPos,3) + points(notMinPos,3) - 3*(points(minPos,2)-points(notMinPos,2))/(points(minPos,1)-points(notMinPos,1));
    d2 = sqrt(d1^2 - points(minPos,3)*points(notMinPos,3));
    if isreal(d2)
        t = points(notMinPos,1) - (points(notMinPos,1) - points(minPos,1))*((points(notMinPos,3) + d2 - d1)/(points(notMinPos,3) - points(minPos,3) + 2*d2));
        minPos = min(max(t,points(minPos,1)),points(notMinPos,1));
    else
        minPos = mean(points(:,1));
    end
    return;
end

xmin = min(points(:,1));
xmax = max(points(:,1));

% Compute Bounds of Interpolation Area
if nargin < 3
    xminBound = xmin;
end
if nargin < 4
    xmaxBound = xmax;
end

% Constraints Based on available Function Values
A = zeros(0,order+1);
b = zeros(0,1);
for i = 1:nPoints
    if imag(points(i,2))==0
        constraint = zeros(1,order+1);
        for j = order:-1:0
            constraint(order-j+1) = points(i,1)^j;
        end
        A = [A;constraint];
        b = [b;points(i,2)];
    end
end

% Constraints based on available Derivatives
for i = 1:nPoints
    if isreal(points(i,3))
        constraint = zeros(1,order+1);
        for j = 1:order
            constraint(j) = (order-j+1)*points(i,1)^(order-j);
        end
        A = [A;constraint];
        b = [b;points(i,3)];
    end
end

% Find interpolating polynomial
params = A\b;

% Compute Critical Points
dParams = zeros(order,1);
for i = 1:length(params)-1
    dParams(i) = params(i)*(order-i+1);
end

if any(isinf(dParams))
    cp = [xminBound;xmaxBound;points(:,1)].';
else
    cp = [xminBound;xmaxBound;points(:,1);roots(dParams)].';
end

% Test Critical Points
fmin = inf;
minPos = (xminBound+xmaxBound)/2; % Default to Bisection if no critical points valid
for xCP = cp
    if imag(xCP)==0 && xCP >= xminBound && xCP <= xmaxBound
        fCP = polyval(params,xCP);
        if imag(fCP)==0 && fCP < fmin
            minPos = real(xCP);
            fmin = real(fCP);
        end
    end
end
% Plot Situation
if doPlot
    figure(1); clf; hold on;

    % Plot Points
    plot(points(:,1),points(:,2),'b*');

    % Plot Derivatives
    for i = 1:nPoints
        if isreal(points(i,3))
            m = points(i,3);
            b = points(i,2) - m*points(i,1);
            plot([points(i,1)-.05 points(i,1)+.05],...
                [(points(i,1)-.05)*m+b (points(i,1)+.05)*m+b],'c.-');
        end
    end

    % Plot Function
    x = min(xmin,xminBound)-.1:(max(xmax,xmaxBound)+.1-min(xmin,xminBound)-.1)/100:max(xmax,xmaxBound)+.1;
    size(x)
    for i = 1:length(x)
        f(i) = polyval(params,x(i));
    end
    plot(x,f,'y');
    axis([x(1)-.1 x(end)+.1 min(f)-.1 max(f)+.1]);

    % Plot Minimum
    plot(minPos,fmin,'g+');
    if doPlot == 1
        pause(1);
    end
end
end


