% Template for integrating additional solvers
% Complete the file by adding your own code. 
% 
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_template'.
%
%
function [out] = template(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
   error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end

if options_general.mode
    AtA = A;
    Atb = b;
    mv = 1;
end

%%% Initialize CPU time
t0 = tic;
%%%
[n, p] = size(A);
%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end

if ~options_general.mode
    %%% un-pack Gram matrix if available; otherwise, compute Gram matrix.
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
        if  mv == 1
            AtA = A'*A;
        end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

%%% initialize gradient

if mv == 1
   grad =@(x) 2 * (AtA * x - Atb); 
else
   grad =@(x) 2 * A' * (A * x - b); 
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) (x' * (AtA * x - 2 * Atb));  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) (x' * (AtA * x) - 2 * x' * Atb); 
     else
        f = @(x) norm(A * x - b)^2;
     end
end

% *DEFINE ALL QUANTITIES THAT REMAIN
% CONSTANT OVER ALL ITERATIONS HERE*

% set-up intitial solution and gradient; *CAN BE MODIFIED*
g = -2*Atb;
x = zeros(p, 1);
% assess optimality of initial solution
perf = getL(options_general.perfmeas, g, x);
check = check_termination(t0, options_general, perf, g, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% 
%%% iterate until stopping criteria are not fulfilled
while ~check.stop
    
    %*****************************************%
    % FILL-IN YOUR CODE HERE THAT GENERATES A %
    % A NEW ITERATE 'x_new'                   %  
    %*****************************************% 
      
    % Take Step
    x = x_new;
    g = grad(x);
    
    % assess optimality of new iterate and check stopping criteria
    perf = getL(options_general.perfmeas, g, x);
    check = check_termination(t0, options_general, perf, g, x, check.fprev, check.xprev);
    perf = perf(~isnan(perf));
    ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
    
end

%%% quit & return

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end