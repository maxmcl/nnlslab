% Template for passing solver-specific options
% (complementary to the file 'template.m').
%
% Inputs              
%
% 'optionname1':	value1 
% 'optionname2': 	value2 
% 'optionname3': 	value3
%  
%  etc.
%
%  

function options = opt_template(varargin)

p = inputParser;

% assess validity of the specifications, see help('validateattributes')

validoption1 = @(x)validateattributes(x,'*classes*','*attributes*');
p.addParamValue('optionname1', '*Put default value here*', validoption1);

validoption2 = @(x)validateattributes(x,'*classes*','*attributes*');
p.addParamValue('optionname2', '*Put default value here*', validoption2);

validoption3 = @(x)validateattributes(x,'*classes*','*attributes*');
p.addParamValue('optionname3', '*Put default value here*', validoption3);

% FILL-IN CODE FOR MORE OPTIONS HERE % 

%

p.parse(varargin{:});
options = p.Results;