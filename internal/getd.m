function[dx] = getd(x, xstar)

dx = norm(xstar - x); 