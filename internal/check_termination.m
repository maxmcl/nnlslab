% t0 -- cputime at the beginning
% options_general
% perf -- after calling function for performance measures
% grad --- gradient
% x --- current iterate
% fprev --- objective at previous iteration
% xprev --- iterate at previous iteration

function[check] = check_termination(t0, options_general, perf, grad, x, fprev, xprev)
global f;

reason = [];
tolreturn = [];
fcur = inf;

stop = 0;
if toc(t0) > options_general.maxcpu
    stop = 1;
    reason = 0; % code: reason=0 (maximum cpu time exceeded) 
end

stopcrit = options_general.stopcrit;
tol = options_general.tol;

[nr, nc] = size(stopcrit);

for i =1:nr
   tempstop = zeros(nc, 1); 
   
    for j = 1:nc
        
        critnumber = stopcrit(i,j);
        tolval = tol(i,j);
        
        if ~isnan(perf(critnumber))
            switch critnumber
                case 1
                    if perf(critnumber) < tolval
                        tolreturn = [tolreturn perf(critnumber)];
                        tempstop(j) = 1;
                        reason = [reason 1];
                    end
                case 2
                    fcur = perf(critnumber);
                    relobj = fprev - fcur;
                    if relobj < tolval
                        tempstop(j) = 1;
                        reason = [reason 2];
                        tolreturn = [tolreturn relobj];
                    end
                case 3
                    %%% * this block appears twice * %%%
                    relx = norm(x - xprev)/norm(0.5 * (x + xprev));
                    if relx < tolval
                        tempstop(j) = 1;
                        reason = [reason 3];
                        tolreturn = [tolreturn relx];
                    end
                    %%%
            end
        else
            switch critnumber
                case 1
                    kktopt = getkktopt(grad, x);
                    if kktopt < tolval
                       tempstop(j) = 1;
                       reason = [reason 1];
                       tolreturn = [tolreturn kktopt];
                    end 
                case 2
                     fcur = f(x);
                     relobj =  fprev - fcur;
                     if relobj < tolval
                       tempstop(j) = 1; 
                       reason = [reason 2];
                       tolreturn = [tolreturn relobj];
                     end      
                case 3
                    %%% * this block appears twice * %%%
                    relx = norm(x - xprev)/norm(0.5 * (x + xprev));
                    if relx < tolval
                        tempstop(j) = 1;
                        reason = [reason 3];
                        tolreturn = [tolreturn relx];
                    end
                    %%%
            end
        end
    end
   
    
    if(all(tempstop == 1)) 
        stop = 1;
    end
end



check.stop = stop;
%%% order
[sorted, ix] = sort(reason);
check.reason = reason(ix);
if ~isempty(tolreturn)
    if min(sorted) == 0
        ix = ix - 1;
        ix(sorted == 0) = [];
        check.tol = tolreturn(ix);
    else
        check.tol = tolreturn(ix);
    end
else
    check.tol = [];
end
%%% to be re-used at next call
check.xprev = x;
check.fprev = fcur;





