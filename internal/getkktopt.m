function[kktopt] = getkktopt(grad, x)

num = abs(grad) .* (grad > eps);
score = num;

Pc = num < eps;

if sum(Pc) > 0
    score(Pc) = num(Pc);
end

if sum(~Pc) > 0
   score(~Pc) = num(~Pc) ./ x(~Pc);
end

Pc = num < eps | score < 1;
P  = ~Pc;

if sum(P) > 0
   kktP = max(abs(x(P)));
else
   kktP = 0;
end

if sum(Pc) > 0
   kktPc = max(abs(grad(Pc)));
else
   kktPc = 0; 
end

N = x < -eps;

if sum(N) > 0
   kktN = max(abs(x(N)));
else
   kktN = 0; 
end

kktopt = max([kktP kktPc kktN]);

end