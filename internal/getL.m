function[perf] = getL(perfmeas, grad, x)
global f;

perf = NaN(1, 3);
%%% entry 1: --- KKT 
%%% entry 2: --- obj. 
%%% entry 3: --- error x - x^{\ast} 

for i = 1:length(perfmeas)
 
    if length(perfmeas{i}) > 1    
        perf(3) = getd(x, perfmeas{i});
    else
        if length(perfmeas{i} == 1)
            switch perfmeas{i}
                case 1
                perf(1) = getkktopt(grad, x);
                case 2
                perf(2) = f(x);
            end
        else
        continue 
        end
    end
end

