// choldown.cpp : Defines the entry point for the console application.
//

#include "mex.h"
#include "matrix.h"
#include <string.h>
#include <math.h>


/*
% cholesky downdating  
%
% G = A'* A = L * L', where L, L' come from cholesky decomposition
% now  removes kth column from A, denoted by Ak. Gk := Ak' * Ak
% Given L' and k, choldown computes the chol. decomposition of  Gk
% i.e. Lk' * Lk = Gk, without processing of A, G

*/

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{

	/* 
		The plhs[] and prhs[] parameters are vectors that contain pointers to each left-hand side (output) variable and
		each right-hand side (input) variable, respectively. Accordingly, plhs[0] contains a pointer to the first left-hand 
		side argument, plhs[1] contains a pointer to the second left-hand side argument, and so on. Likewise, prhs[0] contains
		a pointer to the first right-hand side argument, prhs[1] points to the second, and so on.

              matlab stores a matrix Mat as an 1D array A, A[0] = Mat(1,1), A[1] = Mat(2,1)...(1st clm, 2nd clm)
	       C stores a matrix 1st row, 2nd row....
       */
       
       double *Lt, *Lkt;
	double **Temp, **T; 
	mwSize k,p,i,j,count;
       double a,b,r,c,s;

  
	Lt = mxGetPr(prhs[0]); 
       p = mxGetN(prhs[0]);          /* Lt in R^(p,p) */
	k = mxGetScalar(prhs[1]) - 1; /* kth clm -> idx k-1 */
	
  
	plhs[0] = mxCreateDoubleMatrix(p-1,p-1, mxREAL);
	Lkt = mxGetPr(plhs[0]);
	
       Temp = (double**)malloc(sizeof(double*)*p);
       if(Temp == NULL)
               mexErrMsgTxt("Malloc Temp Failed");
       for (i = 0; i < p; i++)
       {
           Temp[i] = (double*)malloc(sizeof(double)*(p-1)); 
                      if(Temp[i] == NULL)
               mexErrMsgTxt("Malloc Tempi Failed");       
       }
	/** drop the kth column **/
	count = 0;
       for (j = 0; j < p-1; j++)
       {
              if ( j == k )
                 count += p;
              for (i = 0;  i < p; i++)
                  Temp[i][j] = Lt[count++];
       }

       
       /** Givens Rotations **/
       T = (double**)malloc(sizeof(double*)*2);
           if(T == NULL)
               mexErrMsgTxt("Malloc T Failed");
       T[0] = (double*)malloc(sizeof(double)*(p-1));
            if(T[0] == NULL)
               mexErrMsgTxt("Malloc T0 Failed");
       T[1] = (double*)malloc(sizeof(double)*(p-1));
            if(T[1] == NULL)
               mexErrMsgTxt("Malloc T1 Failed");
       for (i = 0; i < p-1; i++)
       {
              T[0][i] = 0; T[1][i] = 0;
       }
       
	for (i = k; i < p-1; i++)
       {
              /*set Givens rotation matrix H*/
              a = Temp[i][i]; b = Temp[i+1][i];
                 
              r=0;
              for (j = 0; j < p; j++)
                r += pow(Lt[(i+1)*p+j],2);
              for (j = 0; j < i; j++)
                r -= pow(Temp[j][i],2);
              r = sqrt(r);
                 
              c = r*a / (a*a + b*b);
              s = r*b / (a*a + b*b);
                                    
              /* rotate */
             
             for (j = i; j < p-1; j++)
                 T[0][j] =  c * Temp[i][j] + s * Temp[i+1][j];
             for (j = i+1; j < p-1; j++)
                 T[1][j] = -s * Temp[i][j] + c * Temp[i+1][j];

             for (j = i; j < p-1;  j++)
             {
                  Temp[i][j]   = T[0][j];
                  Temp[i+1][j] = T[1][j];
                  T[0][j] = 0;
                  T[1][j] = 0;
             }                            
       }

       
              
       /* drop the last row */
       for (j = 0; j< p-1; j++)
           for ( i = 0; i<p-1; i++)
           {
                   Lkt[j*(p-1) + i ] = Temp[i][j];
           }
       
	for (i = 0; i < p; i++)
           free(Temp[i]);	
	free(Temp);
	
	free(T[0]);free(T[1]);free(T);
}
