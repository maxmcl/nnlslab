% Name of the NNLS solver:
% Interior point method based on a log-barrier. 
% PCG (preconditioned conjugate gradient with a diagonal
% preconditioner) is used to (approximately) solve the linear systems
% associated with each Newton system. The use of PCG 
% in this code is an adaption of code available from  
% http://www.stanford.edu/~boyd/l1_ls/. 
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_logbarrier'.
 
function [out] = logbarrier_pcg(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
    error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end
   
if options_general.mode
   AtA = A;
   AtAdiag = diag(AtA);
   Atb = b;
   mv = 1;
end


%%% Initialize CPU time
t0 = tic;
%%%
[n, p] = size(A);

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end
 
if ~options_general.mode
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
        AtAdiag = diag(AtA);
    else
        if options_general.usegram
            AtA = A'*A;
            AtAdiag = diag(AtA);
        else
            AtAdiag = sum(A.^2)';
            mv = 2;
        end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

    
    if mv == 1
        grad = @(x) (AtA * x - Atb);
        AXfunc = @(x, d, pp) AtA * x + d .* x; 
    else
        grad = @(x) A' * (A * x - b);
        AXfunc = @(x, d, pp) A' * (A * x) + d .* x;
    end


global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) x' * (AtA * x) - 2 * x' * Atb; 
     else
        f = @(x) norm(A * x - b)^2;
     end
end

%%%
barrier = @(x, gamma) -1/gamma * sum(log(x));  
%%%
fbarrier = @(x, gamma) f(x) + barrier(x, gamma);
%%%
Mfunc = @(x, d, pp) x .* pp;
%%% 

alpha = 0.01; beta = 0.95; % parameters for the stepsize selection
FLAG = 1; 
   
x = (1E-4)*ones(p,1); 
descent = zeros(p, 1);

if(sum(x <= 0) == 0)
    gradbarrier = -1./x;
else
    error('Valid range for x has been left');
end
 
gradf = grad(x);

GammaVal = (gradf .* x);
%%% 
if ~all(gradf > 0)
    GammaVal = 1 / max(abs(GammaVal(gradf < 0))); 
else
    GammaVal = options_specific.mu;
end

GammaValMax = options_specific.GammaValMax; 
tolinner = options_specific.tolinner; 
mu = options_specific.mu;
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% 

%%%

if isempty(GammaVal)
    out.xopt = x;
    out.err = f(x);
    out.ConvSpeed = ConvSpeed;
    out.check = check;
    return;
end
     
gradobj =  gradf + 1/GammaVal * gradbarrier;

obj = fbarrier(x, GammaVal); 


% to prevent cycling
maxcounter = 100;

while GammaVal <= GammaValMax && (~check.stop)
  
  stopold = Inf;
  counter = 0;

  while FLAG && (~check.stop)
     RHS = -gradobj;
     D = gradbarrier.^2/GammaVal;
     % compute the (truncated) Newton descent direction using pcg,
     % following Koh, Kim and Boyd.
     prb = AtAdiag + D;

    % set pcg tolerance (relative)
    normg   = norm(gradobj);
    gap = getkktopt(gradf, x);
    pcgtol  = min(1e-1, options_specific.pcgtol * gap/min(1, normg));
    

    if 1
        [descent, pflg, prelres, pitr, presvec] = ...
            pcg(AXfunc, RHS, pcgtol, options_specific.pcgmaxiter, Mfunc,...
            [],descent, D, 1./prb);
    end
    

    %     
     
     % get the stepsize with the Armijo rule
     xold = x;
     % get gradient and Hessian at current point
     betavec = beta.^(0:200);
     index = descent<0;
     if(sum(index)>0)
        MaxStep=min(-x(index)./descent(index)); 
     else
        MaxStep=1;
     end
     
     if MaxStep < 1
        MaxStep=max(betavec(betavec < MaxStep));
     end
     
     if  ~isempty(MaxStep)
         t = min(1, MaxStep);
     else
         t = eps / 2;
     end
  
     FLAGSTEP = 1; inner = gradf' * descent;
     while FLAGSTEP
        x = xold + t*descent;
    
        if all(x > 0)
           newobj =  fbarrier(x, GammaVal);
        else
           newobj = inf;
        end
        
        if  newobj > obj + alpha * t * inner
            t = beta * t;
        else
            FLAGSTEP = 0;
        end
        % quit if no significant progress can be made. 
        if  t <= eps 
            FLAGSTEP = 0;
            FLAG = 0;
            GammaVal = GammaValMax + 1;
            continue;
        end
     end
     % update gradient
     gradf = grad(x);
     gradbarrier = -1./x;
     gradobj =  gradf + 1/GammaVal * gradbarrier;
     
     perf = getL(options_general.perfmeas, gradf, x);
     check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
     perf = perf(~isnan(perf));
     ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
     
     
     stop = abs(gradobj'*descent);
     %disp(stop)
     if stop < tolinner
            FLAG = 0;
         else
           if ~(stop < 0.25 * stopold)
              counter = counter + 1;
              if counter == maxcounter
                 FLAG = 0; 
              end
           else
               counter = 0;
               stopold = stop;
               xmem = x;
           end     
     end
  end
  %%% go to memorized iterate
  if counter == maxcounter
     x = xmem;
     gradf = grad(x);
     gradbarrier = -1./x;
     %%%
     perf = getL(options_general.perfmeas, gradf, x);
     perf = perf(~isnan(perf));
     ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
     %%%
  end
  %%%
  FLAG = 1;
  GammaVal = GammaVal * mu;
  %%% !! update gradient !!
  gradobj =  gradf + 1/GammaVal * gradbarrier;
  %%%
 
  
end

%%% QUIT and RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end











  

