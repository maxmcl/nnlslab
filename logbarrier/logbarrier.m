% Name of the NNLS solver:
% Interior point method based on a log-barrier. 
%
%
% Input
%
% A --- coefficient matrix,
%
% b --- observation vector,
%
% options_general --- specification of options as returned by 'initopt_general',
%
% options_specific --- specification of options as returned by 'opt_logbarrier'.

function [out] = logbarrier(A, b, options_general, options_specific)

if max(cellfun(@(z) length(z), options_general.perfmeas)) > size(A, 2)
    error('Dimension mismatch of A and the minimizer x')
end

if ~all(size(options_general.stopcrit) == size(options_general.tol))
   error('Structure of stopcrit not compatible with structure of tol')
end
   
if options_general.mode
   AtA = A;
   Atb = b;
   sherman = 0;
   mv = 1;
end


%%% Initialize CPU time
t0 = tic;
%%%
[n, p] = size(A);

%%%
if ~options_general.mode
    if options_general.mv == 0
        if 1.1 * 2 * n < p
            mv = 2;
        else
            mv = 1;
        end
    else
        mv = options_general.mv;
    end
end

sherman = options_specific.sherman;
if sherman < 0
   if (n^2 * p + n^3) < 0.25 * p^3 
       sherman = 1;  
   else
       sherman = 0;
   end
end
 
if ~options_general.mode
    %%%
    if ~isempty(options_general.gram)
        AtA = options_general.gram;
        if ~all(size(AtA) == [p p])
            error('wrong input for the Gram matrix');
        end
        options_general.gram = [];
    else
       if ~sherman
           AtA = A'*A;
       end
    end
end
%%%

%initialize variables
if ~options_general.mode
    Atb = A' * b;
end

% 
if ~sherman
     if  issparse(AtA) && p^2 > intmax 
         sparseindexing = 1;
     else
         sparseindexing = 0;
         diagind = sub2ind([p,p], 1:p, 1:p)';
         AtAdiag = diag(AtA);
     end
    
    if mv == 1
        grad = @(x) (AtA * x - Atb);
    else
        grad = @(x) A' * (A * x - b);
    end
else
    Attilde = A';
    diagind = sub2ind([n,n], 1:n, 1:n)';
    grad = @(x) A' * (A * x - b);
end

%%% Initialize objective
global f;
if options_general.mode 
    f = @(x) x' * (AtA * x - 2 * Atb);  
else
     if mv == 1 && options_specific.mvobj == 1
          f = @(x) x' * (AtA * x) - 2 * x' * Atb; 
     else
        f = @(x) norm(A * x - b)^2;
     end
end

%%%
barrier = @(x, gamma) -1/gamma * sum(log(x));  
%%%
fbarrier = @(x, gamma) f(x) + barrier(x, gamma);
%%%    
    
alpha = 0.01; beta=0.95; % parameters for the stepsize selection
FLAG = 1; 
   
x = (1E-4)*ones(p,1); 

% initialize gradient here
if(sum(x <= 0) == 0)
    gradbarrier = -1./x;
else
    error('Valid range for x has been left');
end
 
gradf = grad(x);


GammaVal = (gradf .* x);
%%% see options_specific for an explanation
if ~all(gradf > 0)
    GammaVal = 1 / max(abs(GammaVal(gradf < 0))); %%%
else
    GammaVal = options_specific.mu;
end

GammaValMax = options_specific.GammaValMax; %%% 
tolinner = options_specific.tolinner; 
mu = options_specific.mu;
%%%
perf = getL(options_general.perfmeas, gradf, x);
check = check_termination(t0, options_general, perf, gradf, x, inf, x + inf);
perf = perf(~isnan(perf));
ConvSpeed =  [0 perf];
% 

%%%

if isempty(GammaVal)
    out.xopt = x;
    out.err = f(x);
    out.ConvSpeed = ConvSpeed;
    out.check = check;
    return;
end
     
gradobj =  gradf + 1/GammaVal * gradbarrier;

obj = fbarrier(x, GammaVal); 


% to prevent cycling
maxcounter = 100;

while GammaVal <= GammaValMax && (~check.stop)
  
 
  stopold = Inf;
  counter = 0;
  while FLAG && (~check.stop)
     RHS = -gradobj;
     D = gradbarrier.^2/GammaVal;
     % compute the Newton descent direction
     if ~sherman
         if sparseindexing
              AtA = AtA  + spdiags(D, 0, p, p);
         else    
             AtA(diagind) = (D + AtAdiag);
         end
         descent = AtA \ RHS;
         
        if sparseindexing
           AtA = AtA - spdiags(D, 0, p, p);  
        else
           AtA(diagind) = AtAdiag;
        end
         
        
     else
         RHStilde = RHS./D;
         for j=1:p
             Attilde(j,:) = A(:,j) / D(j);
         end
         M = A * Attilde;
         diagM = M(diagind) + 1;
         if (max(diagM) > 1E12)
             % re-scale (symmetrized) for better conditioning
             for j=1:n
                 M(:,j) = M(:,j) / sqrt(diagM(j));
             end
             for j=1:n
                 M(j,:) = M(j,:) / sqrt(diagM(j));
             end
             descent =  RHStilde -  Attilde  *  ((M \ ((A * RHStilde)./sqrt(diagM)))./sqrt(diagM));
         else
             M(diagind) = diagM;
             descent = RHStilde -  Attilde  *  (M \ (A * RHStilde));
         end
     end
     % get the stepsize with the Armijo rule
     xold = x;
     % get gradient and Hessian at current point
     betavec = beta.^(0:200);
     index = descent<0;
     if(sum(index)>0)
        MaxStep=min(-x(index)./descent(index)); 
     else
        MaxStep=1;
     end
     
     if MaxStep < 1
        MaxStep=max(betavec(betavec < MaxStep));
     end
     
     if  ~isempty(MaxStep)
         t = min(1, MaxStep);
     else
         t = eps / 2;
     end
  
     FLAGSTEP = 1; inner = gradf' * descent;
     while FLAGSTEP
        x = xold + t*descent;
    
        if all(x > 0)
           newobj =  fbarrier(x, GammaVal);
        else
           newobj = inf;
        end
        
        if  newobj > obj + alpha * t * inner
            t = beta * t;
        else
            FLAGSTEP = 0;
        end
        % quit if no significant progress can be made. 
        if  t <= eps 
            FLAGSTEP = 0;
            FLAG = 0;
            GammaVal = GammaValMax + 1;
            continue;
        end
     end
     % update gradient
     gradf = grad(x);
     gradbarrier = -1./x;
     gradobj =  gradf + 1/GammaVal * gradbarrier;
     
     perf = getL(options_general.perfmeas, gradf, x);
     check = check_termination(t0, options_general, perf, gradf, x, check.fprev, check.xprev);
     perf = perf(~isnan(perf));
     ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
     
  
     stop = abs(gradobj'*descent);
     %
     if stop < tolinner
            FLAG = 0;
         else
           if ~(stop < 0.25 * stopold)
              counter = counter + 1;
              if counter == maxcounter
                 FLAG = 0; 
              end
           else
               counter = 0;
               stopold = stop;
               xmem = x;
           end     
     end
  end
  %%% go to memorized iterate
  if counter == maxcounter
     x = xmem;
     gradf = grad(x);
     gradbarrier = -1./x;
     perf = getL(options_general.perfmeas, gradf, x);
     perf = perf(~isnan(perf));
     ConvSpeed =  [ConvSpeed; [toc(t0) perf]];
  end
  %%%
  FLAG = 1;
  GammaVal = GammaVal * mu;
  %%%
  gradobj =  gradf + 1/GammaVal * gradbarrier;
  %%%
  
end

%%% QUIT and RETURN

out.xopt = x;
out.err = f(x);
out.ConvSpeed = ConvSpeed; 
out.check = check;

end











  

