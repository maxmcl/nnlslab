% Function to set-up options specific to
% the interior method based on a log-barrier (with PCG).
%
% Inputs
%
% 'mvobj'   ---  scheme to be used for comparison of objective values
%              	'1': (only used if simultaneously 'options_general.mv = 1');
%                   f(x) = x' (A' * A) * x - 2 x' (A  * b) + norm(b).^2
%                   with (A' * A), (A  * b), norm(b).^2 pre-computed and    
%                   kept in storage. [favourable if A is a tall matrix]
%               
%              	'2': f(x) = norm(A * x).^2 (default).
%
% 'GammaValMax' --- 1/Desired accuracy in terms of the KKT optimality condition
%                   (default: 1/GammaValMax = 1E-12)                    
%    
%  'mu' 	--- tuning parameter (>1) as in Boyd and Vandenberghe (2004), Ch.11.3, p.569.
%                   (default: 20) 
%
%  'tolinner'   --- Required accuracy for each centering step (default: 1E-8)   
%
%  'pcgtol'  --- Required accuracy for each application of PCG (default: 1E-3).
%
%  'pcgmax'  --- Maximum number of iterations for each application of PCG (default: 5000).
             
function options = opt_logbarrier_pcg(varargin)
p = inputParser;

% 

validmvobj = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive','<=',2});
p.addParamValue('mvobj', 2, validmvobj);

validGammaValMax = @(x)validateattributes(x,{'numeric'},{'scalar','positive'});
p.addParamValue('GammaValMax',10^12,validGammaValMax);

validmu = @(x)validateattributes(x,{'numeric'},{'scalar','positive','>',1});
p.addParamValue('mu', 20, validmu);

validtolinner = @(x)validateattributes(x,{'numeric'},{'scalar','positive'});
p.addParamValue('tolinner',10^-8,validtolinner);

%%% Additional parameters

validpcgtol = @(x)validateattributes(x,{'numeric'},{'scalar','positive'});
p.addParamValue('pcgtol', 10^-3, validpcgtol);

validpcgmaxiter = @(x)validateattributes(x,{'numeric'},{'scalar','integer','positive'});
p.addParamValue('pcgmaxiter', 5000, validpcgmaxiter);

%%%

p.parse(varargin{:});
options = p.Results;




